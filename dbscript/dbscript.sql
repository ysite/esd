/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.5.19 : Database - esd
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`esd` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `esd`;

/*Table structure for table `authority` */

DROP TABLE IF EXISTS `AUTHORITY`;

CREATE TABLE `AUTHORITY` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USERID` bigint(20) NOT NULL,
  `ROLE` varchar(16) COLLATE utf8_bin NOT NULL,
  `UPDATES` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_AUTHORITY_USER_ROLE` (`USERID`,`ROLE`) USING BTREE,
  CONSTRAINT `FK_AUTHORITY_USER` FOREIGN KEY (`USERID`) REFERENCES `user` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

/*Data for the table `authority` */

insert  into `AUTHORITY`(`ID`,`USERID`,`ROLE`,`UPDATES`) values (1,1,'ROLE_ADMIN',0),(2,1,'ROLE_ENGINEER',0),(3,1,'ROLE_USER',0),(4,2,'ROLE_ENGINEER',0),(5,2,'ROLE_USER',0),(6,3,'ROLE_USER',0);

/*Table structure for table `history` */

DROP TABLE IF EXISTS `HISTORY`;

CREATE TABLE `HISTORY` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PIPELINEID` bigint(20) NOT NULL,
  `PIPELINENAME` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `STATIONID` bigint(20) NOT NULL,
  `STATIONNAME` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `MONITORTIME` datetime NOT NULL,
  `ENDTIME` datetime DEFAULT NULL,
  `STATTIME` datetime DEFAULT NULL,
  `RESULT` varchar(8) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '0未使用 1 正常 -1 告警',
  `VALUEE` varchar(16) COLLATE utf8_bin NOT NULL COMMENT '监测结果值 0|403',
  `DURATION` int(11) DEFAULT '1' COMMENT '持续时间长度，单位分钟',
  PRIMARY KEY (`ID`,`PIPELINEID`),
  UNIQUE KEY `UK_HISTORY_PSRT` (`PIPELINEID`,`STATIONID`,`STATTIME`,`RESULT`),
  KEY `IDX_HISTORY_TIME` (`MONITORTIME`) USING BTREE,
  KEY `IDX_HISTORY_PIPELINE` (`PIPELINENAME`) USING BTREE,
  KEY `IDX_HISTORY_STATION` (`STATIONNAME`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin
/*!50100 PARTITION BY KEY (PIPELINEID)
(PARTITION p0 ENGINE = InnoDB,
 PARTITION p1 ENGINE = InnoDB,
 PARTITION p2 ENGINE = InnoDB,
 PARTITION p3 ENGINE = InnoDB,
 PARTITION p4 ENGINE = InnoDB,
 PARTITION p5 ENGINE = InnoDB,
 PARTITION p6 ENGINE = InnoDB,
 PARTITION p7 ENGINE = InnoDB,
 PARTITION p8 ENGINE = InnoDB,
 PARTITION p9 ENGINE = InnoDB) */;

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `MENU`;

CREATE TABLE `MENU` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(64) COLLATE utf8_bin NOT NULL,
  `URL` varchar(128) COLLATE utf8_bin NOT NULL,
  `ROLE` varchar(32) COLLATE utf8_bin NOT NULL,
  `DESC` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `SORT` int(11) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `menu` */

insert  into `MENU`(`ID`,`NAME`,`URL`,`ROLE`,`DESC`,`SORT`) values (1,'账号管理','admin/account.html','ROLE_ADMIN','账号增删改查',20),(2,'实时概览','user/summary.html','ROLE_USER','实时概览展示',1),(3,'历史数据','usr/history.html','ROLE_USER','历史数据查询',5),(4,'流水线配置','engineer/pipeline.html','ROLE_ENGINEER','生产线设备配置',10),(5,'工位配置','engineer/station.html','ROLE_ENGINEER','工位设备配置',15),(6,'实时详情','user/detail.html','ROLE_USER','实时详情展示',2);

/*Table structure for table `monitor` */

DROP TABLE IF EXISTS `MONITOR`;

CREATE TABLE `MONITOR` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PIPELINEID` bigint(20) NOT NULL,
  `PIPELINENAME` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `STATIONID` bigint(20) NOT NULL,
  `STATIONNAME` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `MONITORTIME` datetime NOT NULL,
  `RESULT` varchar(8) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '0未使用 1 正常 -1 告警',
  `VALUEE` varchar(16) COLLATE utf8_bin NOT NULL COMMENT '监测数据值 0|403',
  `DURATION` int(11) DEFAULT '1' COMMENT '持续时间长度，单位分钟',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_MONITOR_PSD` (`PIPELINEID`,`STATIONID`,`MONITORTIME`,`RESULT`),
  KEY `IDX_MONITOR_TIME` (`MONITORTIME`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Table structure for table `pipeline` */

DROP TABLE IF EXISTS `PIPELINE`;

CREATE TABLE `PIPELINE` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `NUMBER` varchar(8) COLLATE utf8_bin NOT NULL,
  `STATE` smallint(1) NOT NULL DEFAULT '1' COMMENT '1正常0删除',
  `STATIONS` smallint(11) DEFAULT '0',
  `CREATETIME` datetime DEFAULT NULL,
  `DESCC` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `UPDATES` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_PIPELINE_NAME_NUMBER` (`NAME`,`NUMBER`,`STATE`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `pipeline` */

insert  into `PIPELINE`(`ID`,`NAME`,`NUMBER`,`STATE`,`STATIONS`,`CREATETIME`,`DESCC`,`UPDATES`) values (34,'A03','03',1,29,'2014-12-03 20:34:25','A03-Line',0),(35,'A04','04',1,17,'2014-12-03 20:34:41','A04-Line',0),(36,'A05','05',1,22,'2014-12-03 20:35:05','A05-Line',0),(37,'A06','06',1,19,'2014-12-03 20:35:16','A06-Line',0),(38,'A07','07',1,41,'2014-12-03 20:35:31','A07-Line',0),(39,'A08','08',1,41,'2014-12-03 20:35:40','A08-Line',0);

/*Table structure for table `station` */

DROP TABLE IF EXISTS `STATION`;

CREATE TABLE `STATION` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `PIPELINEID` bigint(20) NOT NULL,
  `NUMBER` int(16) NOT NULL,
  `STATE` smallint(6) NOT NULL DEFAULT '0' COMMENT ' 1正常 0 删除',
  `ADDRESS` varchar(16) COLLATE utf8_bin NOT NULL COMMENT '设备地址 1',
  `SUBADDRESS` varchar(16) COLLATE utf8_bin NOT NULL COMMENT '传感器地址 2',
  `CREATETIME` datetime DEFAULT NULL,
  `DESCC` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `UPDATES` int(11) DEFAULT '0',
  `ROWW` smallint(11) DEFAULT '1',
  `COLL` smallint(11) DEFAULT '1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_STATION_NUMBER_PIPELINE` (`PIPELINEID`,`NUMBER`),
  UNIQUE KEY `UK_STATION_ADDRESS` (`ADDRESS`,`SUBADDRESS`),
  KEY `IDX_STATION_CREATETIME` (`CREATETIME`),
  CONSTRAINT `FK_STATION_PIPELINE` FOREIGN KEY (`PIPELINEID`) REFERENCES `pipeline` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `station` */

insert  into `STATION`(`ID`,`NAME`,`PIPELINEID`,`NUMBER`,`STATE`,`ADDRESS`,`SUBADDRESS`,`CREATETIME`,`DESCC`,`UPDATES`,`ROWW`,`COLL`) values (111,'C/A卡入',34,2,0,'36','2','2014-12-23 23:36:00','A03-2	C/A卡入',0,2,12),(112,'面板投入',34,1,0,'35','1','2014-12-23 23:36:00','A03-1	面板投入',0,2,8),(113,'主板投入1',34,3,0,'36','4','2014-12-23 23:36:00','A03-3	主板投入1',0,2,12),(115,'灯管线插入1',34,4,0,'35','2','2014-12-23 23:36:00','A03-4	灯管线插入1',0,2,16),(116,'Speaker投入',34,6,0,'37','3','2014-12-23 23:36:00','A03-6	Speaker投入',0,2,20),(118,'面板装配',34,7,0,'38','4','2014-12-23 23:36:00','A03-7	面板装配',0,2,24),(119,'Screw 固定1',34,8,0,'38','3','2014-12-23 23:36:00','A03-8	Screw 固定1',0,2,24),(120,'Sheet投入',34,9,0,'39','2','2014-12-23 23:36:00','A03-9	Sheet投入',0,2,28),(121,'连接线整理',34,10,0,'39','4','2014-12-23 23:36:00','A03-10	连接线整理',0,2,28),(122,'Tape粘贴1',34,11,0,'39','3','2014-12-23 23:36:00','A03-11	Tape粘贴1',0,2,28),(123,'Screw固定2',34,12,0,'40','2','2014-12-23 23:36:00','A03-12	Screw固定2',0,2,34),(124,'Screw固定3',34,13,0,'4','2','2014-12-23 23:36:00','A03-13	Screw固定3',0,2,38),(125,'主板投入2',34,14,0,'36','1','2014-12-23 23:36:00','A03-14	主板投入2',0,1,12),(126,'ID Label投入',34,15,0,'36','3','2014-12-23 23:36:00','A03-15	ID Label投入',0,1,12),(127,'FFC 插入1',34,16,0,'38','2','2014-12-23 23:36:00','A03-16	FFC 插入1',0,1,24),(128,'PCBA定位',34,17,0,'38','1','2014-12-23 23:36:00','A03-17	PCBA定位',0,1,24),(129,'FFC 插入2',34,18,0,'39','1','2014-12-23 23:36:00','A03-18	FFC 插入2',0,1,28),(130,'Tape粘贴2',34,19,0,'40','3','2014-12-23 23:36:00','A03-19	Tape粘贴2',0,1,34),(131,'Screw固定4',34,20,0,'40','1','2014-12-23 23:36:00','A03-20	Screw固定4',0,1,34),(132,'A-Part 准备',39,1,0,'97','3','2014-12-23 23:36:00','A08-1	A-Part 准备',0,2,2),(133,'A-Part 投入',39,2,0,'97','4','2014-12-23 23:36:00','A08-2	A-Part 投入',0,2,2),(134,'M/D投入',39,3,0,'97','2','2014-12-23 23:36:00','A08-3	M/D投入',0,2,4),(135,'FFC投入',39,4,0,'97','1','2014-12-23 23:36:00','A08-4	FFC投入',0,2,4),(136,'摄像头投入',39,5,0,'98','2','2014-12-23 23:36:00','A08-5	摄像头投入',0,2,6),(137,'M/D Scan',39,6,0,'98','1','2014-12-23 23:36:00','A08-6	M/D Scan',0,2,6),(138,'连接线Tape固定',39,7,0,'98','3','2014-12-23 23:36:00','A08-7	连接线Tape固定',0,2,8),(139,'铰链投入固定',39,8,0,'98','4','2014-12-23 23:36:00','A08-8	铰链投入固定',0,2,8),(140,'M/D安装',39,9,0,'99','1','2014-12-23 23:35:00','A08-9 M/D安装',0,2,10),(141,'M/D固定',39,10,0,'99','2','2014-12-23 23:35:00','A08-10	M/D固定',0,2,10),(142,'B-Part投入',39,11,0,'100','2','2014-12-23 23:36:00','A08-11	B-Part投入',0,2,12),(143,'B-Part捏合1',39,12,-1,'100','1','2014-12-23 23:36:00','A08-12	B-Part捏合1',0,2,12),(144,'B-Part捏合2',39,13,0,'100','4','2014-12-23 23:36:00','A08-13	B-Part捏合2',0,2,14),(145,'KBD检查投入',39,14,0,'100','3','2014-12-23 23:36:00','A08-14	KBD检查投入',0,2,14),(146,'主板投入',39,15,0,'101','1','2014-12-23 23:36:00','A08-15	主板投入',0,2,16),(147,'绝缘片粘贴',39,16,0,'101','2','2014-12-23 23:36:00','A08-16	绝缘片粘贴',0,2,16),(148,'SUB 板投入',39,17,0,'101','4','2014-12-23 23:36:00','A08-17	SUB 板投入',0,2,18),(149,'Speaker投入',39,18,0,'101','3','2014-12-23 23:36:00','A08-18	Speaker投入',0,2,18),(150,'Sponge粘贴',39,19,0,'102','2','2014-12-23 23:36:00','A08-19	Sponge粘贴',0,2,20),(151,'散热片投入',39,20,0,'102','3','2014-12-23 23:36:00','A08-20	散热片投入',0,2,22),(152,'连接线插入1',39,21,0,'102','1','2014-12-23 23:36:00','A08-21	连接线插入1',0,2,20),(153,'连接线插入2',39,22,0,'102','4','2014-12-23 23:36:00','A08-22	连接线插入2',0,2,22),(154,'主板固定1',39,23,0,'103','1','2014-12-23 23:36:00','A08-23	主板固定1',0,2,24),(155,'主板固定2',39,24,0,'103','2','2014-12-23 23:36:00','A08-24	主板固定2',0,2,24),(156,'主板固定3',39,25,0,'103','3','2014-12-23 23:36:00','A08-25	主板固定3',0,2,26),(157,'Tape粘贴1',39,26,0,'103','4','2014-12-23 23:36:00','A08-26	Tape粘贴1',0,2,26),(158,'HDD投入',39,27,0,'104','2','2014-12-23 23:36:00','A08-27	HDD投入',0,2,28),(159,'光驱投入',39,28,0,'104','1','2014-12-23 23:36:00','A08-28	光驱投入',0,2,28),(160,'连接线整理1',39,29,0,'105','2','2014-12-23 23:36:00','A08-29	连接线整理1',0,2,30),(161,'连接线整理2',39,30,0,'105','1','2014-12-23 23:36:00','A08-30	连接线整理2',0,2,30),(162,'连接线整理3',39,31,0,'106','1','2014-12-23 23:36:00','A08-31	连接线整理3',0,2,32),(163,'Tape粘贴2',39,32,0,'106','2','2014-12-23 23:36:00','A08-32	Tape粘贴2',0,2,32),(164,'Scan',39,33,0,'107','3','2014-12-23 23:36:00','A08-33	Scan',0,2,34),(165,'目检',39,34,0,'107','4','2014-12-23 23:36:00','A08-34	目检',0,2,34),(166,'备用1',39,35,0,'107','2','2014-12-23 23:36:00','A08-35	备用1',0,2,36),(167,'备用2',39,36,0,'107','1','2014-12-23 23:36:00','A08-36	备用2',0,2,36),(168,'D-Part投入',39,37,0,'108','2','2014-12-23 23:36:00','A08-37	D-Part投入',0,2,38),(169,'HDD安装',39,38,0,'108','1','2014-12-23 23:36:00','A08-38	HDD安装',0,2,38),(170,'B/C固定1',39,39,0,'108','3','2014-12-23 23:36:00','A08-39	B/C固定1',0,2,40),(171,'B/C固定2',39,40,0,'20','1','2014-12-23 23:36:00','A08-40	B/C固定2',0,2,42),(172,'备用3',39,41,0,'20','2','2014-12-23 23:36:00','A08-41	备用3',0,2,42),(173,'T-com固定1',35,1,0,'49','2','2014-12-23 23:36:00','A04-1 T-com固定1',0,2,2),(174,'T-com固定2',35,2,0,'49','1','2014-12-23 23:36:00','A04-2 T-com固定2',0,2,6),(175,'C/A导电条粘贴',35,3,0,'49','3','2014-12-23 23:36:00','A04-3 C/A导电条粘贴',0,2,6),(176,'M/D Label粘贴',35,4,0,'50','1','2014-12-23 23:36:00','A04-4 M/D Label粘贴',0,2,12),(177,'绝缘垫片粘贴',35,5,0,'51','2','2014-12-23 23:36:00','A04-5 绝缘垫片粘贴 ',0,2,16),(178,'FFC插入1',35,6,0,'52','1','2014-12-23 23:36:00','A04-6 FFC插入1',0,2,24),(179,'FFC插入2',35,7,0,'53','2','2014-12-23 23:36:00','A04-7 FFC插入2\nA04-7',0,2,30),(180,'连接线整理1',35,8,0,'53','1','2014-12-23 23:36:00','A04-8 连接线整理1',0,2,30),(181,'Tape粘贴1',35,9,0,'54','3','2014-12-23 23:36:00','Tape粘贴1 A04-9',0,2,34),(182,'Tape粘贴2',35,10,0,'54','2','2014-12-23 23:36:00','Tape粘贴2 A04-10',0,2,34),(184,'备用2',35,11,0,'56','1','2014-12-23 23:36:00','备用2 A04-12',0,2,44),(187,'灯管线插入1',35,12,0,'51','1','2014-12-23 23:36:00','灯管线插入1 A04-15',0,1,16),(188,'主板投入',35,13,0,'52','2','2014-12-23 23:36:00','主板投入 A04-16',0,1,24),(189,'灯管线插入2',35,14,0,'53','3','2014-12-23 23:36:00','灯管线插入2 A04-17',0,1,30),(190,'面板线插入',35,15,0,'54','1','2014-12-23 23:36:00','面板线插入 A04-18',0,1,34),(192,'备用3',35,16,0,'55','1','2014-12-23 23:36:00','备用3 A04-20',0,1,40),(193,'备用4',35,17,0,'55','2','2014-12-23 23:36:00','A04-21 备用4',0,1,40),(194,'T-com固定1',36,1,0,'65','1','2014-12-23 23:36:00','A05-1	T-com固定1',0,2,4),(195,'T-com固定2',36,2,0,'66','1','2014-12-23 23:36:00','A05-2	T-com固定2',0,2,6),(196,'M/D Label粘贴',36,3,0,'66','2','2014-12-23 23:36:00','A05-3	M/D Label粘贴',0,2,6),(197,'绝缘垫片粘贴',36,4,0,'67','1','2014-12-23 23:36:00','A05-4	绝缘垫片粘贴',0,2,10),(198,'灯管线插入',36,5,0,'67','2','2014-12-23 23:36:00','A05-5	灯管线插入',0,2,10),(199,'M/D Scan',36,6,0,'67','3','2014-12-23 23:36:00','A05-6	M/D Scan',0,2,12),(201,'FFC插入2',36,7,0,'68','1','2014-12-23 23:36:00','A05-8	FFC插入2',0,2,16),(202,'Tape粘贴1',36,8,0,'69','4','2014-12-23 23:36:00','A05-9	Tape粘贴1',0,2,20),(203,'Tape粘贴2',36,9,0,'69','2','2014-12-23 23:36:00','A05-10	Tape粘贴2',0,2,24),(204,'Tape粘贴3',36,10,0,'69','3','2014-12-23 23:36:00','A05-11	Tape粘贴3',0,2,24),(205,'备用1',36,11,0,'70','1','2014-12-23 23:36:00','A05-12	备用1',0,2,28),(206,'备用2',36,12,0,'70','2','2014-12-23 23:36:00','A05-13	备用2',0,2,28),(207,'备用3',36,13,0,'71','1','2014-12-23 23:36:00','A05-14	备用3',0,2,32),(208,'备用4',36,14,0,'72','1','2014-12-23 23:36:00','A05-15	备用4',0,2,36),(209,'备用5',36,15,0,'72','2','2014-12-23 23:36:00','A05-16	备用5',0,2,36),(210,'备用6',36,16,0,'72','4','2014-12-23 23:36:00','A05-17	备用6',0,2,36),(211,'主板投入',36,17,0,'67','4','2014-12-23 23:36:00','A05-18	主板投入',0,1,10),(212,'面板投入',36,18,0,'68','2','2014-12-23 23:36:00','A05-19	面板投入',0,1,12),(213,'主板定位',36,19,0,'68','3','2014-12-23 23:36:00','A05-20	主板定位',0,1,16),(214,'面板线插入',36,20,0,'68','4','2014-12-23 23:36:00','A05-21	面板线插入',0,1,16),(215,'灯管线插入',36,21,0,'69','1','2014-12-23 23:36:00','A05-22	灯管线插入',0,1,20),(216,'备用7',36,22,0,'72','3','2014-12-23 23:36:00','A05-23	备用7',0,1,36),(217,'T-com固定1',37,1,0,'73','1','2014-12-23 23:36:00','A06-1	T-com固定1',0,2,2),(218,'T-com固定2',37,2,0,'74','1','2014-12-23 23:36:00','A06-2	T-com固定2',0,2,6),(219,'C/A导电条粘贴',37,3,0,'74','2','2014-12-23 23:36:00','A06-3	C/A导电条粘贴',0,2,10),(220,'M/D Label粘贴',37,4,0,'74','3','2014-12-23 23:36:00','A06-4	M/D Label粘贴',0,2,10),(221,'绝缘垫片粘贴',37,5,0,'75','4','2014-12-23 23:36:00','A06-5	绝缘垫片粘贴',0,2,18),(222,'灯管线插入1',37,6,0,'76','2','2014-12-23 23:36:00','A06-6	灯管线插入1',0,2,22),(223,'灯管线插入2',37,7,0,'77','1','2014-12-23 23:36:00','A06-7	灯管线插入2',0,2,28),(224,'线整理',37,8,0,'77','2','2014-12-23 23:36:00','A06-8	线整理',0,2,28),(225,'Tape粘贴1',37,9,0,'21','1','2014-12-23 23:36:00','A06-9	Tape粘贴1',0,2,32),(226,'Tape粘贴2',37,10,0,'21','3','2014-12-23 23:36:00','A06-10	Tape粘贴2',0,2,36),(227,'备用1',37,11,0,'22','1','2014-12-23 23:36:00','A06-11	备用1',0,2,44),(228,'备用2',37,12,0,'23','1','2014-12-23 23:36:00','A06-12	备用2',0,2,46),(229,'主板投入',37,13,0,'75','1','2014-12-23 23:36:00','A06-13	主板投入',0,1,14),(230,'FFC插入1',37,14,0,'75','2','2014-12-23 23:36:00','A06-14	FFC插入1',0,1,14),(231,'FFC插入2',37,15,0,'75','3','2014-12-23 23:36:00','A06-15	FFC插入2',0,1,18),(232,'主板定位',37,16,0,'76','1','2014-12-23 23:36:00','A06-16	主板定位',0,1,22),(233,'面板线插入',37,17,0,'77','3','2014-12-23 23:36:00','A06-17	面板线插入',0,1,28),(234,'Tape粘贴3',37,18,0,'21','2','2014-12-23 23:36:00','A06-18	Tape粘贴3',0,1,32),(235,'备用3',37,19,0,'22','2','2014-12-23 23:36:00','A06-19	备用3',0,1,44),(236,'A-Part 准备',38,1,0,'81','2','2014-12-23 23:36:00','A07-1	A-Part 准备',0,2,2),(237,'A-Part 投入',38,2,0,'81','1','2014-12-23 23:36:00','A07-2	A-Part 投入',0,2,2),(238,'M/D投入',38,3,0,'81','3','2014-12-23 23:36:00','A07-3	M/D投入',0,2,4),(239,'FFC投入',38,4,0,'81','4','2014-12-23 23:36:00','A07-4	FFC投入',0,2,4),(240,'摄像头投入',38,5,0,'82','1','2014-12-23 23:36:00','A07-5	摄像头投入',0,2,6),(241,'M/D Scan',38,6,0,'82','2','2014-12-23 23:36:00','A07-6	M/D Scan',0,2,6),(242,'连接线Tape固定',38,7,0,'82','3','2014-12-23 23:36:00','A07-7	连接线Tape固定',0,2,8),(243,'铰链投入固定',38,8,0,'83','1','2014-12-23 23:36:00','A07-8	铰链投入固定',0,2,10),(244,'M/D安装',38,9,0,'83','3','2014-12-23 23:36:00','A07-9	M/D安装',0,2,10),(245,'M/D固定',38,10,0,'83','2','2014-12-23 23:36:00','A07-10	M/D固定',0,2,12),(246,'B-Part投入',38,11,0,'84','1','2014-12-23 23:36:00','A07-11	B-Part投入',0,2,14),(247,'B-Part捏合1',38,12,0,'84','3','2014-12-23 23:36:00','A07-12	B-Part捏合1',0,2,16),(248,'B-Part捏合2',38,13,0,'84','2','2014-12-23 23:36:00','A07-13	B-Part捏合2',0,2,16),(249,'KBD检查投入',38,14,0,'85','1','2014-12-23 23:36:00','A07-14	KBD检查投入',0,2,18),(250,'主板投入',38,15,0,'85','2','2014-12-23 23:36:00','A07-15	主板投入',0,2,18),(251,'绝缘片粘贴',38,16,0,'85','3','2014-12-23 23:36:00','A07-16	绝缘片粘贴',0,2,20),(252,'SUB 板投入',38,17,0,'85','4','2014-12-23 23:36:00','A07-17	SUB 板投入',0,2,20),(253,'Speaker投入',38,18,0,'86','3','2014-12-23 23:36:00','A07-18	Speaker投入',0,2,22),(254,'Sponge粘贴',38,19,0,'86','4','2014-12-23 23:36:00','A07-19	Sponge粘贴',0,2,22),(255,'散热片投入',38,20,0,'86','2','2014-12-23 23:36:00','A07-20	散热片投入',0,2,24),(256,'连接线插入1',38,21,0,'86','1','2014-12-23 23:36:00','A07-21	连接线插入1',0,2,24),(257,'连接线插入2',38,22,0,'87','3','2014-12-23 23:36:00','A07-22	连接线插入2',0,2,26),(258,'主板固定1',38,23,0,'87','4','2014-12-23 23:36:00','A07-23	主板固定1',0,2,26),(259,'主板固定2',38,24,0,'87','2','2014-12-23 23:36:00','A07-24	主板固定2',0,2,28),(260,'主板固定3',38,25,0,'87','1','2014-12-23 23:36:00','A07-25	主板固定3',0,2,28),(261,'Tape粘贴1',38,26,0,'88','3','2014-12-23 23:36:00','A07-26	Tape粘贴1',0,2,30),(262,'HDD投入',38,27,0,'88','1','2014-12-23 23:36:00','A07-27	HDD投入',0,2,32),(263,'光驱投入',38,28,0,'88','2','2014-12-23 23:36:00','A07-28	光驱投入',0,2,32),(264,'连接线整理1',38,29,0,'89','1','2014-12-23 23:36:00','A07-29	连接线整理1',0,2,34),(265,'连接线整理2',38,30,0,'89','2','2014-12-23 23:36:00','A07-30	连接线整理2',0,2,34),(266,'连接线整理3',38,31,0,'90','1','2014-12-23 23:36:00','A07-31	连接线整理3',0,2,36),(267,'Tape粘贴2',38,32,0,'90','2','2014-12-23 23:36:00','A07-32	Tape粘贴2',0,2,36),(268,'Scan',38,33,0,'91','3','2014-12-23 23:36:00','A07-33	Scan',0,2,38),(269,'目检',38,34,0,'91','1','2014-12-23 23:36:00','A07-34	目检',0,2,40),(270,'备用1',38,35,0,'91','2','2014-12-23 23:36:00','A07-35	备用1',0,2,40),(271,'备用2',38,36,0,'92','3','2014-12-23 23:36:00','A07-36	备用2',0,2,42),(272,'D-Part投入',38,37,0,'92','1','2014-12-23 23:36:00','A07-37	D-Part投入',0,2,44),(273,'HDD安装',38,38,0,'92','2','2014-12-23 23:36:00','A07-38	HDD安装',0,2,44),(274,'B/C固定',38,39,0,'93','2','2014-12-23 23:36:00','A07-39	B/C固定',0,2,46),(276,'备用3',38,41,0,'82','4','2014-12-23 23:36:00','备用',0,2,8),(281,'备用4',38,40,0,'93','1','2014-12-23 23:36:00','备用',0,2,46),(282,'备用1',34,21,0,'4','1','2014-12-23 23:36:00','备用1',0,1,38),(283,'备用2',34,22,0,'37','1','2014-12-23 23:36:00','备用2',0,2,20),(285,'备用3',34,23,0,'33','4','2014-12-23 23:36:00','备用3',0,1,2),(286,'备用4',34,24,0,'34','4','2014-12-23 23:36:00','备用4',0,1,4),(287,'备用5',34,25,0,'33','3','2014-12-23 23:36:00','备用5',0,1,2),(288,'备用6',34,26,0,'34','2','2014-12-23 23:36:00','备用6',0,1,4),(290,'备用7',34,27,0,'33','1','2014-12-23 23:36:00','备用7',0,2,2),(291,'备用8',34,28,0,'34','3','2014-12-23 23:36:00','备用8',0,2,4),(292,'备用9',34,29,0,'33','2','2014-12-23 23:36:00','备用9',0,2,2),(293,'备用10',34,30,0,'34','1','2014-12-23 23:36:00','备用10',0,2,4),(299,'灯管线插入2',34,5,0,'37','2','2014-12-23 23:36:00','A03-5	灯管线插入2',0,1,20);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `USER`;

CREATE TABLE `USER` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACCOUNT` varchar(64) COLLATE utf8_bin NOT NULL,
  `PASSWORD` varchar(128) COLLATE utf8_bin NOT NULL,
  `MOBILE` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `EMAIL` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `LOCKED` smallint(1) NOT NULL DEFAULT '0',
  `EXPIRATION` datetime NOT NULL DEFAULT '2020-12-30 00:00:00',
  `ENABLED` smallint(1) NOT NULL DEFAULT '1',
  `UPDATES` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_USER_ACCOUNT` (`ACCOUNT`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `user` */

insert  into `USER`(`ID`,`NAME`,`ACCOUNT`,`PASSWORD`,`MOBILE`,`EMAIL`,`LOCKED`,`EXPIRATION`,`ENABLED`,`UPDATES`) values (1,'admin','admin','admin','','admin@123.com',0,'2020-12-30 00:00:00',1,0),(2,'engineer','engineer','engineer','','engineer@126.com',0,'2020-12-30 00:00:00',1,0),(3,'user','user','user','','user@126.com',0,'2020-12-30 00:00:00',1,0);

/* Trigger structure for table `monitor` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `INSERT_MONITOR_TRIGGER` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `INSERT_MONITOR_TRIGGER` AFTER INSERT ON `monitor` FOR EACH ROW BEGIN
    
    UPDATE STATION SET STATE=NEW.RESULT,CREATETIME=DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i:00') WHERE ID=NEW.STATIONID;
    
   INSERT INTO HISTORY(PIPELINEID,PIPELINENAME,STATIONID,STATIONNAME,MONITORTIME,ENDTIME,STATTIME,RESULT,VALUEE,DURATION)
     VALUES(NEW.PIPELINEID,NEW.PIPELINENAME,NEW.STATIONID,NEW.STATIONNAME,NOW(),NOW(),DATE_FORMAT(NOW(),'%Y-%m-%d %H:00:00'),NEW.RESULT,NEW.VALUEE,NEW.DURATION)
    ON DUPLICATE KEY UPDATE DURATION=DURATION+1,ENDTIME=NOW();
END */$$


DELIMITER ;

/* Trigger structure for table `monitor` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `UPDATE_MONITOR_TRIGGER` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `UPDATE_MONITOR_TRIGGER` AFTER UPDATE ON `monitor` FOR EACH ROW BEGIN
    
    UPDATE STATION SET STATE=NEW.RESULT,CREATETIME=DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i:00') WHERE ID=NEW.STATIONID;
    
    INSERT INTO HISTORY(PIPELINEID,PIPELINENAME,STATIONID,STATIONNAME,MONITORTIME,ENDTIME,STATTIME,RESULT,VALUEE,DURATION)
     VALUES(NEW.PIPELINEID,NEW.PIPELINENAME,NEW.STATIONID,NEW.STATIONNAME,NOW(),NOW(),DATE_FORMAT(NOW(),'%Y-%m-%d %H:00:00'),NEW.RESULT,NEW.VALUEE,NEW.DURATION)
    ON DUPLICATE KEY UPDATE DURATION=DURATION+1,ENDTIME=NOW();
END */$$


DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
