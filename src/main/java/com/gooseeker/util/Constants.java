package com.gooseeker.util;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.userdetails.UserDetails;

import com.gooseeker.modbus.bean.SensorResultBean;
import com.gooseeker.modbus.bean.StationInfoBean;

public final class Constants {
	//host url ,etc:www.yahoo.com,regxlib.com/Default.aspx 
	public static final String HOST_REGEX = "[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?";
	
	//用户缓存
	public static final Map<String, UserDetails> USER_MAP = new ConcurrentHashMap<String, UserDetails>(100);
	//资源url-role缓存
	public static final Map<String, List<ConfigAttribute>> RESOURCE_MAP = new ConcurrentHashMap<String, List<ConfigAttribute>>();
	
	public static int PAGE_NUM = 10;
	
	public static final String HOST_CODE = "host";
	public static final String NAME_CODE = "name";
	
	public static final String DEFAULT_TAG_COMMA_HALF = ";";
	public static final String DEFAULT_TAG_COMMA_FULL = "；";
	
	public static final int INSERT_DB_ERROR = -1001;
	public static final int UPDATE_DB_ERROR = -1002;
	public static final int DELETE_DB_ERROR = -1003;
	public static final int CREATE_DB_ERROR = -1004;
	
	public static final ExecutorService THREAD_AUTO_TAG_POOL = Executors.newCachedThreadPool();
	public final static String DEFAULT_SPLIT_WORD = "\b";
	public final static String COMMON_SPLIT_WORD = "_";
	public final static String EXPORT_FILE_SUFFIX = ".xlsx";
	public final static int EXPORT_RECORD_NUM = 100000;

	public static final String CHINESE_SPLIT = "[。|！|？|：|；|，|.|!|?|:|,|\\s+]";
	
	/**
	 * 默认串口，可配置 
	 * TODO 添加配置文件，用于动态加载
	 */
	public static String DEFAULT_COM_PORT = "COM3";
	/**
	 * 读取数据延迟时间，单位ms
	 */
	public static int DELAY_READ_DATA = 200;
	/**
	 * 打开串口的等待超时时间
	 */
	public static int DEFAULT_OPEN_TIMEOUT = 10000;
	/**
	 * 默认波特率
	 */
	public static int DEFAULT_COM_BUAD_RATE = 9600;
	/**
	 * 默认数据位
	 */
	public static int DEFAULT_COM_DATA_BIT = 8;
	/**
	 * 默认停止位
	 */
	public static int DEFAULT_COM_STOP_BIT = 1;
	/**
	 * 默认校验位
	 */
	public static int DEFAULT_COM_PARITY = 0;
	/**
	 * ADC电压范围
	 */
	public static int ADC_VOLTAGE_RANGE = 5;
	/**
	 * 正常阈值
	 */
	public static float PASS_THRESHOLD = 0.5f;
	/**
	 * 告警阈值
	 */
	public static float FAIL_THRESHOLD = 0.5f;
	/**
	 * ADC精度
	 */
	public static int ADC_PRECISION = 10;
	/**
	 * 根据阈值范围转换成正常与告警时实际采集到的值
	 */
	public static int PASS_ADC_THRESHOLD_VALUE = 0;
	
	public static int FAIL_ADC_THRESHOLD_VALUE = 0;
	//命令重发次数
	public static int CMD_RESEND_TIMES = 3;
	/*
	 * 命令重发间隔，单位ms
	 */
	public static int CMD_RESEND_PERIOD = 50;
	
	/**
	 * 正常时多次采集电压差的和的阈值
	 */
	public static int PASS_RESULT_DELTA = 100;
	/**
	 * 报警时多次采集电压差的和的阈值
	 */
	public static int FAIL_RESULT_DELTA = 200;
	
	public static int UNUSE_RESULT_DELTA = 20;
	
	public static final int DEFAULT_RECV_BUFFER_SIZE = 21;
	
	public static boolean IS_CYCLE_FINISH = false;
	public static int PARSE_THREAD_POOL_SIZE = 20;
	public static ExecutorService PARSE_THREAD_POOL = Executors.newFixedThreadPool(PARSE_THREAD_POOL_SIZE);
	public static ScheduledExecutorService CHECK_THREAD_POOL = Executors.newScheduledThreadPool(2);
	public static ConcurrentHashMap<StationInfoBean, List<SensorResultBean>> DATA_MAP = new ConcurrentHashMap<StationInfoBean, List<SensorResultBean>>();
}
