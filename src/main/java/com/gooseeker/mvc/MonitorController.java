package com.gooseeker.mvc;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.gooseeker.business.PipelineService;
import com.gooseeker.business.StationService;
import com.gooseeker.dao.beans.Monitor;
import com.gooseeker.dao.beans.Pipeline;
import com.gooseeker.dao.beans.Station;
import com.gooseeker.dao.beans.Summary;
import com.gooseeker.util.Constants;
import com.gooseeker.util.JsonUtil;
import com.gooseeker.util.SeekerUser;

@Controller
public class MonitorController 
{
    private StationService stationService;
    private PipelineService pipelineService;
    
    @Autowired
    public void setPipelineService(PipelineService pipelineService)
    {
        this.pipelineService = pipelineService;
    }


    @Autowired
    public void setStationService(StationService stationService)
    {
        this.stationService = stationService;
    }


    @RequestMapping("/user/monitor.html")
	public ModelAndView monitor(){
        SeekerUser user = authenticate();
		if (null == user)
        {
            return new ModelAndView("login");
        }
		
		List<Pipeline> pipelines = pipelineService.getAllPipelines();
		Map<String, List<Station>> stationsMap = getStationStatus();
		//打标签使用的java结束
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("monitor");
		modelAndView.addObject("menus", user.getMenus());
		modelAndView.addObject("pipelines", pipelines);
		modelAndView.addObject("stationsMap", stationsMap);
		modelAndView.addObject("name", user.getName());
		return modelAndView;
	}
    
    @RequestMapping("/user/detail.html")
	public ModelAndView detail(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException{
        SeekerUser user = authenticate();
		if (null == user)
        {
            return new ModelAndView("login");
        }
		response.setContentType("application/json;charset=utf-8");
		String pipelineId = request.getParameter("pipelineId");
		if("".equals(pipelineId))
		{
			pipelineId = null;
		}
		String keyword = request.getParameter("keyword");
		if("".equals(keyword))
		{
			keyword = null;
		}
		
//		if(pipelineId != null && !"".equals(pipelineId))
//		{
//			String[] temp = pipelineId.split("_");
//			if(temp.length > 1)
//			{
//				pipelineId = temp[1];
//			}
//		}
//		Map<String, List<Station>> stationsMap = pipelineService.getStationStatus(pipelineId,keyword);
		Map<String, List<Station>> stationsMap = pipelineService.getStationStatus(null,keyword);
		TreeMap<String, List<Station>> stationTreeMap = new TreeMap<String, List<Station>>(stationsMap);
		//打标签使用的java结束
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("detail");
		modelAndView.addObject("menus", user.getMenus());
		modelAndView.addObject("pipelineId",pipelineId);
//		modelAndView.addObject("pipelines", pipelines);
		modelAndView.addObject("stationsMap", stationTreeMap);
		modelAndView.addObject("name", user.getName());
		return modelAndView;
	}


    private Map<String, List<Station>> getStationStatus()
    {
        Map<String, List<Station>> stationsMap = stationService.queryAllStationInfo(null,null);
		List<Summary> summaries = pipelineService.queryPipelineSummary();
		for (Summary summary : summaries)
        {
            if (null == summary || 0 == summary.getMonitors().size())
            {
                continue;
            }
            long pipeId = summary.getPipelineId();
            List<Monitor> monitorList = summary.getMonitors();
            List<Station> stationList = stationsMap.get(pipeId);
            if (null == monitorList 
                || 0 == monitorList.size()
                || null == stationList
                || 0 == stationList.size())
            {
                continue;
            }
                
            for (Station station : stationList)
            {
                for (Monitor monitor : monitorList)
                {
                    if (station.getId() == monitor.getStationId())
                    {
                        station.setState(monitor.getResult());
                    }
                }
            }
        }
        return stationsMap;
    }


    private SeekerUser authenticate()
    {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		SeekerUser user = (SeekerUser)Constants.USER_MAP.get(authentication.getName());
		return user;
    }
	
    @RequestMapping("/user/refreshpipeline.html")
    public ModelAndView monitorRefresh(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        SeekerUser user = authenticate();
        if (null == user)
        {
            return new ModelAndView("login");
        }
        response.setContentType("application/json;charset=utf-8");
        String pipelineId = request.getParameter("pipelineId");
        if("".equals(pipelineId))
        {
            pipelineId = null;
        }
        String keyword = request.getParameter("keyword");
        if("".equals(keyword))
        {
            keyword = null;
        }
        
        if(pipelineId != null && !"".equals(pipelineId))
        {
            String[] temp = pipelineId.split("_");
            if(temp.length > 1)
            {
                pipelineId = temp[1];
            }
        }
//      List<Pipeline> pipelines = pipelineService.getAllPipelines();
        Map<String, List<Station>> stationsMap = pipelineService.getStationStatus(pipelineId,keyword);
//        Iterator<String> it = stationsMap.keySet().iterator();
//		while(it.hasNext())
//		{
//			String key = it.next();
//			System.out.print(key+"  ");
//			for(Station station : stationsMap.get(key))
//			{
//				System.out.print(station.getState()+" ");
//			}
//			System.out.println();
//		}
		
        StringBuffer jsonResp = new StringBuffer();
        Set<Entry<String, List<Station>>> stationsSet = stationsMap.entrySet();
        List<Station> allStations = new ArrayList<Station>();
        for (Entry<String, List<Station>> entry : stationsSet)
        {
            allStations.addAll(entry.getValue());
        }

        jsonResp.append("{\"result\":");
        jsonResp.append(JsonUtil.getJSONString(allStations)).append(",");
        jsonResp.append("\"success\":\"").append(true).append("\"}");
        
       // System.out.println(jsonResp);
        PrintWriter pw = response.getWriter();
        pw.println(jsonResp.toString());
        return null;
    }
    
    @RequestMapping("/user/refresh.html")
    public ModelAndView refresh(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        SeekerUser user = authenticate();
        if (null == user)
        {
            return new ModelAndView("login");
        }
        String pipelineId = request.getParameter("pipelineId");
		String keyword = request.getParameter("keyword");
        Map<String, List<Station>> stationsMap = pipelineService.getStationStatus(pipelineId,keyword);
        StringBuffer jsonResp = new StringBuffer();
        Set<Entry<String, List<Station>>> stationsSet = stationsMap.entrySet();
        List<Station> allStations = new ArrayList<Station>();
        for (Entry<String, List<Station>> entry : stationsSet)
        {
            allStations.addAll(entry.getValue());
        }

        jsonResp.append("{\"result\":");
        jsonResp.append(JsonUtil.getJSONString(allStations)).append(",");
        jsonResp.append("\"success\":\"").append(true).append("\"}");
        
        PrintWriter pw = response.getWriter();
        pw.println(jsonResp.toString());
        return null;
    }
}
