package com.gooseeker.mvc;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.gooseeker.business.StationService;
import com.gooseeker.dao.beans.Station;
import com.gooseeker.modbus.CmdUtils;
import com.gooseeker.modbus.ModbusUtils;
import com.gooseeker.modbus.bean.ComUserData;
import com.gooseeker.util.Constants;
import com.gooseeker.util.DBSeekerException;
import com.gooseeker.util.JsonUtil;
import com.gooseeker.util.SeekerUser;

@Controller
public class StationController 
{
	private static Logger logger = Logger.getLogger(StationController.class);
	
	private StationService stationService;
	
	@Autowired
	public void setStationService(StationService stationService) {
		this.stationService = stationService;
	}


	@RequestMapping("/engineer/station.html")
	public ModelAndView station(){
		boolean isAdmin = false;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		SeekerUser user = (SeekerUser)Constants.USER_MAP.get(authentication.getName());
		if(user == null)
		{
			return new ModelAndView("login");
		}
		for(GrantedAuthority ga : authentication.getAuthorities()){
			if("ROLE_ADMIN".equals(ga.getAuthority()))
			{
				isAdmin = true;
			}
		}
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("station");
		modelAndView.addObject("menus", user.getMenus());
		modelAndView.addObject("name", user.getName());
		modelAndView.addObject("isAdmin", isAdmin);
		return modelAndView;
	}
	
	@RequestMapping("/engineer/stationQuery.html")
	public ModelAndView stationQuery(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		SeekerUser user = (SeekerUser)Constants.USER_MAP.get(authentication.getName());
		if(user == null)
		{
			return new ModelAndView("login");
		}
		long  userId=user.getUserId();
		boolean flag=true;
		response.setContentType("application/json;charset=utf-8");
		String keyword = request.getParameter("keyword");
		int pageNum =request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));
		PrintWriter pw = response.getWriter();
		List<Station> stationList = stationService.findStations4Page(keyword,(pageNum-1)*Constants.PAGE_NUM,Constants.PAGE_NUM);
		int count = stationService.findStations4PageCount(keyword);
		String jsonData = JsonUtil.getJSONString(stationList);
		pw.println("{\"jsonData\":" +jsonData + ",\"userId\":\""+userId+ "\",\"success\":"+flag+ ",\"total\":\""+count+"\","+"\"pageSize\":\""+Constants.PAGE_NUM+"\","+"\"pageNo\":\""+pageNum+"\"}");
		
		return null;
	}
	
	@RequestMapping("/engineer/stationDelete.html")
	public ModelAndView stationDelete(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		SeekerUser user = (SeekerUser)Constants.USER_MAP.get(authentication.getName());
		if(user == null)
		{
			return new ModelAndView("login");
		}
		
		response.setContentType("application/json;charset=utf-8");
		String ids = request.getParameter("ids");
		int result = 0;
		if(ids != null && !"".equals(ids))
		{
			String[] idList = ids.trim().split(",");
			for(String id : idList)
			{
				if("".equals(id))
				{
					continue;
				}
				result = stationService.deleteStation(Long.parseLong(id));
			}
		}
//		long id = Long.parseLong(request.getParameter("id") == null ? "0" : request.getParameter("id"));
		PrintWriter pw = response.getWriter();
		boolean flag=true;
		if(result > 0)
		{
			String jsonData = "删除成功";
			pw.println("{\"jsonData\":\"" +jsonData+ "\",\"success\":"+flag+"}");
			
			logger.info("工位删除成功,result="+result);
		}
		else
		{
			flag=false;
			String jsonData = "删除失败";
			pw.println("{\"jsonData\":\"" +jsonData+ "\",\"success\":"+flag+"}");
			logger.info("工位删除失败,result="+result);
		}
		
		return null;
	}
	
	@RequestMapping("/engineer/stationAdd.html")
	public ModelAndView stationAdd(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		SeekerUser user = (SeekerUser)Constants.USER_MAP.get(authentication.getName());
		if(user == null)
		{
			return new ModelAndView("login");
		}
		boolean flag=true;
		PrintWriter pw = response.getWriter();
		//name:name,number:number,pipelineId:pipelineId,address:address,desc:desc
		String name = request.getParameter("name");
		String number = request.getParameter("number");
		long pipelineId = request.getParameter("pipelineId") == null ? 0L : Long.parseLong(request.getParameter("pipelineId"));
		String address = request.getParameter("address");
		String subAddress = request.getParameter("subAddress");
		String col = request.getParameter("col");
		String row = request.getParameter("row");
		String desc = request.getParameter("desc");
		try 
		{
			long result = stationService.insertStation(name, pipelineId, number, address,subAddress, desc, row, col);
			
			if(result > 0)
			{
				String jsonData = "新增成功";
				pw.println("{\"jsonData\":\"" +jsonData+ "\",\"success\":"+flag+"}");
				logger.info("工位新增成功,result="+result);
			}
			else
			{
				flag = false;
				String jsonData = "新增失败";
				pw.println("{\"jsonData\":\"" +jsonData+ "\",\"success\":"+flag+"}");
				logger.info("工位新增失败,result="+result);
			}
		} catch (DBSeekerException e) {
			flag = false;
			String jsonData = "新增失败";
			if(e.getErrorCode() == Constants.INSERT_DB_ERROR)
			{
				jsonData = "不能增加名称和编号或地址重复的工位，请检查。";
			}
			pw.println("{\"jsonData\":\"" +jsonData+ "\",\"success\":"+flag+"}");
			
			logger.error("不能增加名称和编号或地址重复的工位，请检查。result="+Constants.INSERT_DB_ERROR);
		}
		
		
		return null;
	}
	
	@RequestMapping("/engineer/getStation.html")
	public ModelAndView getStation(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		SeekerUser user = (SeekerUser)Constants.USER_MAP.get(authentication.getName());
		if(user == null)
		{
			return new ModelAndView("login");
		}
		long id=request.getParameter("id") == null ? 0 : Long.parseLong(request.getParameter("id"));
		boolean flag=true;
		PrintWriter pw = response.getWriter();
		response.setContentType("application/json;charset=utf-8");
		Station station = stationService.getStation(id);
		String jsonData = JsonUtil.getJSONString(station);
		pw.println("{\"jsonData\":" +jsonData + ",\"success\":"+flag+"}");
		
		return null;
	}
	
	@RequestMapping("/engineer/resetAddress.html")
	public ModelAndView resetAddress(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SeekerUser user = (SeekerUser)Constants.USER_MAP.get(authentication.getName());
        if(user == null)
        {
            return new ModelAndView("login");
        }
        boolean flag=true;
        PrintWriter pw = response.getWriter();
        //{id:id,name:name,number:number,pipelineId:pipelineId,address:address,desc:desc}
        int address = Integer.parseInt(request.getParameter("address"));
        String newAddress = request.getParameter("newAddress");
        int result = 0;
        try
        {
            ComUserData newAddr = new ComUserData();
            newAddr.setData(newAddress);
            byte[] cmd = CmdUtils.setSlaveAddr(address, newAddr);
            ModbusUtils.getInstance().send(cmd);
            result = stationService.updateAddress(request.getParameter("address"), newAddress);
            
        }
        catch (Exception e)
        {
        	logger.error("重置地址成功,messgae="+e.getMessage());
            e.printStackTrace();
        }
        
        if(result > 0)
        {
            String jsonData = "更新成功";
            pw.println("{\"jsonData\":\"" +jsonData+ "\",\"success\":"+flag+"}");
            
            logger.info("重置地址成功,result="+result);
        }
        else
        {
            flag = false;
            String jsonData = "更新失败";
            pw.println("{\"jsonData\":\"" +jsonData+ "\",\"success\":"+flag+"}");
            
            logger.info("重置地址失败,result="+result);
        }
        
        return null;
    }
	@RequestMapping("/engineer/stationUpdate.html")
	public ModelAndView stationUpdate(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		SeekerUser user = (SeekerUser)Constants.USER_MAP.get(authentication.getName());
		if(user == null)
		{
			return new ModelAndView("login");
		}
		boolean flag=true;
		PrintWriter pw = response.getWriter();
		//{id:id,name:name,number:number,pipelineId:pipelineId,address:address,desc:desc}
		long id = request.getParameter("id") == null ? 0 : Long.parseLong(request.getParameter("id"));
		String name = request.getParameter("name");
		String number = request.getParameter("number");
		String pipelineId = request.getParameter("pipelineId");
		String address = request.getParameter("address");
		String subAddress = request.getParameter("subAddress");
		String desc = request.getParameter("desc");
		String row = request.getParameter("row");
		String col = request.getParameter("col");
		int result = stationService.updateStation(id, name, number, pipelineId, address,subAddress,desc, row, col);
		
		if(result > 0)
		{
			String jsonData = "更新成功";
			pw.println("{\"jsonData\":\"" +jsonData+ "\",\"success\":"+flag+"}");
		}
		else
		{
			flag = false;
			String jsonData = "更新失败";
			pw.println("{\"jsonData\":\"" +jsonData+ "\",\"success\":"+flag+"}");
		}
		
		return null;
	}
	
	@RequestMapping("/engineer/listStation.html")
	public ModelAndView listStationById(HttpServletRequest request, HttpServletResponse response) throws Exception
	{

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		SeekerUser user = (SeekerUser)Constants.USER_MAP.get(authentication.getName());
		if(user == null)
		{
			return new ModelAndView("login");
		}
		long  userId=user.getUserId();
		boolean flag=true;
		response.setContentType("application/json;charset=utf-8");
		long pipelineId = Long.parseLong(request.getParameter("pipelineId"));
//		int pageNum =request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));
		PrintWriter pw = response.getWriter();
		List<Station> stationList = stationService.listStations4Page(pipelineId,0,100);
		int count = stationService.listStations4PageCount(pipelineId);
		String jsonData = JsonUtil.getJSONString(stationList);
		pw.println("{\"jsonData\":" +jsonData + ",\"userId\":\""+userId+ "\",\"success\":"+flag+ ",\"total\":\""+count+"\"}");
		
		return null;
	
	}
	
}
