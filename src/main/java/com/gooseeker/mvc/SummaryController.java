package com.gooseeker.mvc;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.gooseeker.business.PipelineService;
import com.gooseeker.dao.beans.Pipeline;
import com.gooseeker.dao.beans.Summary;
import com.gooseeker.util.Constants;
import com.gooseeker.util.JsonUtil;
import com.gooseeker.util.SeekerUser;

@Controller
public class SummaryController
{
	private PipelineService pipelineService;
	
	
	@Autowired
	public void setPipelineService(PipelineService pipelineService) {
		this.pipelineService = pipelineService;
	}

	@RequestMapping("/user/summary.html")
	public ModelAndView summary(){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		SeekerUser user = (SeekerUser)Constants.USER_MAP.get(authentication.getName());
		if(user == null)
		{
			return new ModelAndView("login");
		}
		
		//打标签使用的java结束
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("summary");
		modelAndView.addObject("menus", user.getMenus());
		modelAndView.addObject("name", user.getName());
		
		List<Pipeline> pipelines = pipelineService.getAllPipelines();
		modelAndView.addObject("pipelines",pipelines);
		
		return modelAndView;
	}
	
	@RequestMapping("/user/summaryData.html")
	public ModelAndView summaryData(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		SeekerUser user = (SeekerUser)Constants.USER_MAP.get(authentication.getName());
		if(user == null)
		{
			return new ModelAndView("login");
		}
		
		boolean flag = false;
		PrintWriter pw = response.getWriter();
		response.setContentType("application/json;charset=utf-8");
//		List<Summary> summaries = pipelineService.queryPipelineSummary4Day();
		List<Summary> summaries = pipelineService.queryPipelineSummary4Minutes();
		if(summaries.size() > 0)
		{
			flag = true;
		}
		
		String jsonData = JsonUtil.getJSONString(summaries);
		pw.println("{\"jsonData\":" +jsonData + ",\"success\":"+flag+ "}");
		return null;
	}
	
}
