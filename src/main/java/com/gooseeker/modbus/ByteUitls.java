package com.gooseeker.modbus;


public class ByteUitls
{
	public static int byte2Int(byte b)
	{
		int ret = 0;
		
		if (b < 0)
		{
			ret = 0xff - Math.abs(b);
		}
		else
		{
			ret = b;
		}
		
		return ret;
	}
	
	public static String printByteArray(byte[] array, int len)
	{
		StringBuffer sb = new StringBuffer();
		
		if (array != null)
		{
			for (int i = 0; i < len; i++)
			{
				sb.append(CRC16Utils.intToHexStr(array[i]));
				sb.append(" ");
			}
		}
		
		return sb.toString();
	}
	
	public static void main(String[] args)
	{
		/*
		byte b = 120;
		byte[] cmds =
			{ 0x11, 0x04, 0x10, 0x00, 0x00, 0x00, (byte) 0xbd, 0x00, 0x00, 0x00,
					0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x11,
					(byte) 0xfd, 0x5a };
		*/
		//System.out.println(ByteUitls.byte2Int(b));
		
		System.out.println(Math.atan(1));
	}
}
