package com.gooseeker.modbus;

public class ConstValue
{
	/**
	 * 从机最大地址
	 */
	public static final int SLAVE_MAX_ADDR = 247;
	/**
	 * 从机最小地址
	 */
	public static final int SLAVE_MIN_ADDR = 1;
	/**
	 * 一般性成功
	 */
	public static final int SUCCESS = 0;
	
	//=====================传感器状态===============================
	/**
	 * 传感器状态：报警（红灯）
	 */
	public static final int SENSOR_STATUS_FAIL = -1;
	/**
	 * 传感器状态：正常（绿灯）
	 */
	public static final int SENSOR_STATUS_PASS = 1;
	/**
	 * 传感器状态：未使用（不亮）
	 */
	public static final int SENSOR_STATUS_UNUSE = 0;
	/**
	 * 传感器状态：未知
	 */
	public static final int SENSOR_STATUS_UNKNOWN = 0x99;
	
	//=====================错误码===============================
	/**
	 * 端口被占用
	 */
	public static final int ERROR_COM_PORT_OCCUPIED = 0x1000; 
	/**
	 * 端口不存在
	 */
	public static final int ERROR_COM_PORT_NOT_EXIST = 0x1001;
	/**
	 * 打开串口失败
	 */
	public static final int ERROR_COM_OPEN_FAILED = 0x1002;
	/**
	 * 打开串口成功
	 */
	public static final int ERROR_COM_CLOSE_FAILED = 0x1003;
	
	//=====================命令===============================
	/**
	 * 测试设备是否连通
	 */
	public static final int MODBUS_TEST_DEVICE = 1;
	/**
	 * 测试设备连通ACK
	 */
	public static final int MODBUS_TEST_DEVICE_ACK = 2;
	/**
	 * 读取设备序列号（全局唯一地址：7字节）
	 */
	public static final int MODBUS_READ_SNO = 3;
	/**
	 * 读取8路采集数据值
	 */
	public static final int MODBUS_READ_ALL_DATA = 4;
	/**
	 * 设置从机地址
	 */
	public static final int MODBUS_MOD_SLAVE_ADDR = 5;
	/**
	 * 修改波特率
	 */
	public static final int MODBUS_MOD_BUAD_RATE = 6;
	/**
	 * 保存用户数据（长度：16B）
	 */
	public static final int MODBUS_PUT_USER_DATA = 7;
	/**
	 * 读取用户数据（长度：16B）
	 */
	public static final int MODBUS_GET_USER_DATA = 8;
	/**
	 * 读取某一通道的数据
	 */
	public static final int MODBUS_READ_DATA_BY_CHANNEL = 9;
	
	//=====================波特率===============================
	public static final int COM_BUAD_RATE_2400 = 0;
	
	public static final int COM_BUAD_RATE_4800 = 1;
	
	public static final int COM_BUAD_RATE_9600 = 2;
	
	public static final int COM_BUAD_RATE_19200 = 3;
	
	public static final int COM_BUAD_RATE_38400 = 4;
	
	public static final int COM_BUAD_RATE_57600 = 5;
	
	public static final int COM_BUAD_RATE_115200 = 6;
	
	//=====================MODBUS命令字=========================
	/**
	 * 读取模拟量输入关键字 0x04
	 */
	public static final int PROTOCAL_KEYWORD_READ_INPUT = 4;
	/**
	 * 测试连通关键字 0x30
	 */
	public static final int PROTOCAL_KEYWORD_TEST_CONNECT = 48;
	/**
	 * 设置参数关键字（波特率/地址）0x32
	 */
	public static final int PROTOCAL_KEYWORD_SET_PARAM = 50;
	/**
	 * 保存用户数据关键字 0x33
	 */
	public static final int PROTOCAL_KEYWORD_WRITE_DATA = 51;
	/**
	 * 读取用户数据关键字 0x34
	 */
	public static final int PROTOCAL_KEYWORD_READ_DATA = 52;
	/**
	 * 读取SNO关键字 0x35
	 */
	public static final int PROTOCAL_KEYWORD_READ_SNO = 53;
}
