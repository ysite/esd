package com.gooseeker.modbus.bean;

import com.gooseeker.modbus.ConstValue;

public class SensorResultBean
{
	/**
	 * 设备地址
	 */
	private int deviceAddr;
	/**
	 * 传感器地址（工位地址）
	 */
	private int sensorAddr;
	/**
	 * 状态
	 */
	private int state;
	/**
	 * 当前PASS线电压值
	 */
	private short value1;
	/**
	 * 当前FAIL线电压值
	 */
	private short value2;
	
	public SensorResultBean(SensorResultBean bean)
	{
		this.deviceAddr = bean.getDeviceAddr();
		this.sensorAddr = bean.getSensorAddr();
		this.value1 = bean.getValue1();
		this.value2 = bean.getValue2();
		this.state = bean.getState();
	}
	
	public SensorResultBean()
	{
		this.deviceAddr = 0;
		this.sensorAddr = 0;
		this.value1 = 0;
		this.value2 = 0;
		this.state = ConstValue.SENSOR_STATUS_UNKNOWN;
	}
	
	public int getDeviceAddr()
	{
		return deviceAddr;
	}
	public void setDeviceAddr(int deviceAddr)
	{
		this.deviceAddr = deviceAddr;
	}
	public int getSensorAddr()
	{
		return sensorAddr;
	}
	public void setSensorAddr(int sensorAddr)
	{
		this.sensorAddr = sensorAddr;
	}
	public String toString()
	{
		String ret = "[device:" + deviceAddr + "]";
		ret += "[sensor:" + sensorAddr + "]";
		ret += "[value1:" + value1 + "]";
		ret += "[value2:" + value2 + "]";
		
		return ret;
	}
	public short getValue2()
	{
		return value2;
	}
	public void setValue2(short value2)
	{
		this.value2 = value2;
	}
	public short getValue1()
	{
		return value1;
	}
	public void setValue1(short value1)
	{
		this.value1 = value1;
	}

	public int getState()
	{
		return state;
	}

	public void setState(int state)
	{
		this.state = state;
	}
}
