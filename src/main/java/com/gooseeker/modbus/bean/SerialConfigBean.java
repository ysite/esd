package com.gooseeker.modbus.bean;

public class SerialConfigBean
{
	/**
	 * 串口号，可配置 
	 */
	private String serialPort;
	/**
	 * 读取数据延迟时间，单位ms
	 */
	private int delayReadTime;
	/**
	 * 打开串口的等待超时时间
	 */
	private int openTimeout;
	/**
	 * 波特率
	 */
	private int buadrate;
	/**
	 * 数据位
	 */
	private int dataBit;
	/**
	 * 停止位
	 */
	private int stopBit;
	/**
	 * 校验位
	 */
	private int parity;
	public String getSerialPort()
	{
		return serialPort;
	}
	public void setSerialPort(String serialPort)
	{
		this.serialPort = serialPort;
	}
	public int getDelayReadTime()
	{
		return delayReadTime;
	}
	public void setDelayReadTime(int delayReadTime)
	{
		this.delayReadTime = delayReadTime;
	}
	public int getOpenTimeout()
	{
		return openTimeout;
	}
	public void setOpenTimeout(int openTimeout)
	{
		this.openTimeout = openTimeout;
	}
	public int getBuadrate()
	{
		return buadrate;
	}
	public void setBuadrate(int buadrate)
	{
		this.buadrate = buadrate;
	}
	public int getDataBit()
	{
		return dataBit;
	}
	public void setDataBit(int dataBit)
	{
		this.dataBit = dataBit;
	}
	public int getStopBit()
	{
		return stopBit;
	}
	public void setStopBit(int stopBit)
	{
		this.stopBit = stopBit;
	}
	public int getParity()
	{
		return parity;
	}
	public void setParity(int parity)
	{
		this.parity = parity;
	}
}
