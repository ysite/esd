package com.gooseeker.modbus.bean;

/**
 * 串口用户数据
 * @author Administrator
 *
 */
public class ComUserData
{
	/**
	 * 用户数据(用户私有数据、地址信息(地址范围：1~247)、波特率)
	 */
	private String data;
	/**
	 * 数据长度
	 */
	private int len;
	
	public String getData()
	{
		return data;
	}
	public void setData(String data)
	{
		this.data = data;
	}
	public int getLen()
	{
		return len;
	}
	public void setLen(int len)
	{
		this.len = len;
	}
}
