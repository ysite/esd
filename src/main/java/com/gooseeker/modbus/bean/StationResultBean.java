package com.gooseeker.modbus.bean;

public class StationResultBean
{
	private int state;
	
	private String value;
	
	private int times;

	public StationResultBean()
	{
		this.state = 0;
		this.value = "";
		this.times = 1;
	}
	
	public int getState()
	{
		return state;
	}

	public void setState(int state)
	{
		this.state = state;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}
	
	public int getTimes()
	{
		return times;
	}

	public void setTimes(int times)
	{
		this.times = times;
	}
}
