package com.gooseeker.modbus.bean;

import java.util.ArrayList;
import java.util.List;

import com.gooseeker.util.Constants;

/*
 * 存放工位信息
 * 采集卡地址和子地址，子地址为1，2，3，4
 */
public class StationInfoBean
{
	private int address;
	
	private int subAddress;
	
	private long createTime;//记录第一次创建时间
	
	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public int getAddress()
	{
		return address;
	}

	public void setAddress(int address)
	{
		this.address = address;
	}

	public int getSubAddress()
	{
		return subAddress;
	}

	public void setSubAddress(int subAddress)
	{
		this.subAddress = subAddress;
	}
	
	@Override
	public int hashCode() {
		return Integer.parseInt(address+""+subAddress);
	}

	@Override
	public boolean equals(Object obj) {
		StationInfoBean other = (StationInfoBean) obj;
		if (address != other.address)
			return false;
		if (subAddress != other.subAddress)
			return false;
		return true;
	}

	public static void main(String[] args)
	{
		StationInfoBean infoBean = new StationInfoBean();
		infoBean.setAddress(1);
		infoBean.setSubAddress(2);
		infoBean.setCreateTime(System.currentTimeMillis());
		
		Constants.DATA_MAP.put(infoBean, new ArrayList<SensorResultBean>());
		
		StationInfoBean infoBean2 = new StationInfoBean();
		infoBean2.setAddress(1);
		infoBean2.setSubAddress(2);
		infoBean2.setCreateTime(System.currentTimeMillis());
		
		List<SensorResultBean> resultBeans = Constants.DATA_MAP.get(infoBean2);
		System.out.println(resultBeans);
	}
}
