package com.gooseeker.modbus;

public class ErrorCode
{
	/**
	 * 端口被占用
	 */
	public static final int ERROR_COM_PORT_OCCUPIED = 0x1000; 
	/**
	 * 端口不存在
	 */
	public static final int ERROR_COM_PORT_NOT_EXIST = 0x1001;
	/**
	 * 串口打开失败
	 */
	public static final int ERROR_COM_OPEN_FAILED = 0x1002;
	/**
	 * 串口关闭失败
	 */
	public static final int ERROR_COM_CLOSE_FAILED = 0x1003;
}
