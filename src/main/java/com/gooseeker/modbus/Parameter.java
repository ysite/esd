package com.gooseeker.modbus;

public class Parameter 
{
	private String port;
	private int baudRate;
	private int dataBits;
	private int parity;
	private int stopBits;
	private int timeout;
	private float passThreshold;//正常时电压阈值范围
	private float failthreshold;//报警时电压阈值范围
	private int adcPrecision;
	private int voltageRange;
	private int pageNum; //页面展示条数
	private int resendTime;//命令重发次数
	private int resendPeriod;//命令重发间隔,单位ms
	private int passResultDelta;//正常时多次采集结果的detla和的阈值
	private int failResultDelta;//报警时多次采集结果的detla和的阈值
	private int unuseResultDelta;//未使用时多次采集结果的delta和的阈值
	
	public float getPassThreshold()
	{
		return passThreshold;
	}
	public void setPassThreshold(float passThreshold)
	{
		this.passThreshold = passThreshold;
	}
	public float getFailthreshold()
	{
		return failthreshold;
	}
	public void setFailthreshold(float failthreshold)
	{
		this.failthreshold = failthreshold;
	}
	public int getAdcPrecision()
	{
		return adcPrecision;
	}
	public void setAdcPrecision(int adcPrecision)
	{
		this.adcPrecision = adcPrecision;
	}
	
	public int getPageNum() 
	{
		return pageNum;
	}
	public void setPageNum(int pageNum) 
	{
		this.pageNum = pageNum;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public int getBaudRate() {
		return baudRate;
	}
	public void setBaudRate(int baudRate) {
		this.baudRate = baudRate;
	}
	public int getDataBits() {
		return dataBits;
	}
	public void setDataBits(int dataBits) {
		this.dataBits = dataBits;
	}
	public int getParity() {
		return parity;
	}
	public void setParity(int parity) {
		this.parity = parity;
	}
	public int getStopBits() {
		return stopBits;
	}
	public void setStopBits(int stopBits) {
		this.stopBits = stopBits;
	}
	public int getTimeout() {
		return timeout;
	}
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	public int getVoltageRange()
	{
		return voltageRange;
	}
	public void setVoltageRange(int voltageRange)
	{
		this.voltageRange = voltageRange;
	}
	public int getResendTime()
	{
		return resendTime;
	}
	public void setResendTime(int resendTime)
	{
		this.resendTime = resendTime;
	}
	public int getResendPeriod()
	{
		return resendPeriod;
	}
	public void setResendPeriod(int resendPeriod)
	{
		this.resendPeriod = resendPeriod;
	}
	public int getPassResultDelta()
	{
		return passResultDelta;
	}
	public void setPassResultDelta(int passResultDelta)
	{
		this.passResultDelta = passResultDelta;
	}
	public int getFailResultDelta()
	{
		return failResultDelta;
	}
	public void setFailResultDelta(int failResultDelta)
	{
		this.failResultDelta = failResultDelta;
	}
	public int getUnuseResultDelta()
	{
		return unuseResultDelta;
	}
	public void setUnuseResultDelta(int unuseResultDelta)
	{
		this.unuseResultDelta = unuseResultDelta;
	}
	
}
