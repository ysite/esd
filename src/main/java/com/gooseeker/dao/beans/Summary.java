package com.gooseeker.dao.beans;

import java.util.List;

public class Summary {
	private long pipelineId;
	private String pipelineName;
	private String piplineNumber;
	
	private int alarms = 0;
	private int unuseds = 0;
	private int normals = 0;
	
	private List<Overview> overviews;
	private List<Monitor> monitors;
	public Summary()
	{
	}
	
	public Summary(long pipelineId,String pipelineName,String pipelineNumber,int totals)
	{
		this.pipelineId = pipelineId;
		this.pipelineName = pipelineName;
		this.piplineNumber = pipelineNumber;
		this.unuseds = totals;
	}
	
	public String getPiplineNumber() {
		return piplineNumber;
	}

	public void setPiplineNumber(String piplineNumber) {
		this.piplineNumber = piplineNumber;
	}

	
	
	public long getPipelineId() {
		return pipelineId;
	}

	public void setPipelineId(long pipelineId) {
		this.pipelineId = pipelineId;
	}

	public String getPipelineName() {
		return pipelineName;
	}

	public void setPipelineName(String pipelineName) {
		this.pipelineName = pipelineName;
	}

	public List<Monitor> getMonitors() {
		return monitors;
	}

	public void setMonitors(List<Monitor> monitors) {
		this.monitors = monitors;
	}

	public List<Overview> getOverviews() {
		return overviews;
	}

	public void setOverviews(List<Overview> overviews) {
		this.overviews = overviews;
	}

	public int getAlarms() {
		return alarms;
	}

	public void setAlarms(int alarms) {
		this.alarms = alarms;
	}

	public int getUnuseds() {
		return unuseds;
	}

	public void setUnuseds(int unuseds) {
		this.unuseds = unuseds;
	}

	public int getNormals() {
		return normals;
	}

	public void setNormals(int normals) {
		this.normals = normals;
	}
	
	
}
