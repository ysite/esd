package com.gooseeker.dao;

import com.gooseeker.dao.beans.Station;
import com.gooseeker.util.Constants;
import com.gooseeker.util.DBSeekerException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StationDao extends Dao{
	private String LIST = "listStation";
	private String INSERT_STATION = "insertStation";
	private String FIND_STATIONS_4_PAGE = "findStations4Page";
	private String FIND_STATIONS_4_PAGE_COUNT = "findStations4PageCount";
	private String DELETE_STATION = "deleteStation";
	private String GET_STATION_BY_ID = "getStationById";
	private String UPDATE_STATION = "updateStation";
	private String UPDATE_ADDRESS = "updateAddress";
	private String LIST_STATION_4_PAGE = "listStation4Page";
	private String LIST_STATION_4_PAGE_COUNT = "listStation4PageCount";
	private String FIND_ALL_ADDRESS = "findAllAddress";
	private String QUERY_STATION_BY_ADDRESS = "queryStationByAddress";
	private String GET_STATION_4_MINUTES = "getStations4Minutes";
	
	public List<Station> getAllStations(String pipelineId,String keyword)
	{
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("pipelineId", pipelineId);
		parameterMap.put("keyword", keyword);
		
		return getMetacorpora().selectList(LIST,parameterMap);
	}
	
	public List<Station> getStations4Minutes(String startTime)
	{
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("startTime", startTime);
		
		return getMetacorpora().selectList(GET_STATION_4_MINUTES,parameterMap);
	}
	//NAME,PIPELINEID,NUMBER,ADDRESS,CREATETIME,DESCC
	public long insertStation(String name,long pipelineId,String number,String address,String subAddress,String desc, String row, String col) throws DBSeekerException
	{
		try 
		{
			long id = 0L;
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			parameterMap.put("name", name);
			parameterMap.put("pipelineId", pipelineId);
			parameterMap.put("number", number);
			parameterMap.put("address", address);
			parameterMap.put("subAddress", subAddress);
			parameterMap.put("desc", desc);
			parameterMap.put("row", row);
			parameterMap.put("col", col);
			
			int result = getMetacorpora().insert(INSERT_STATION, parameterMap);
			if(result > 0)
			{
				id = Long.parseLong(String.valueOf(parameterMap.get("ID")));
			}
			return id;
		} catch (Exception e) {
			throw new DBSeekerException(e.getMessage(), Constants.INSERT_DB_ERROR);
		}
		
	}
	
	public List<Station> findStations4Page(String keyword,int start,int length)
	{
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("keyword", keyword);
		parameterMap.put("start", start);
		parameterMap.put("length", length);
		
		return getMetacorpora().selectList(FIND_STATIONS_4_PAGE,parameterMap);
	}
	
	public int findStations4PageCount(String keyword)
	{
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("keyword", keyword);
		return getMetacorpora().selectOne(FIND_STATIONS_4_PAGE_COUNT,parameterMap);
	}
	
	public int deleteStation(long id)
	{
		return getMetacorpora().delete(DELETE_STATION,id);
	}
	
	public Station getStationById(long id) {
		return getMetacorpora().selectOne(GET_STATION_BY_ID,id);
	}
	
	public int updateStation(long id, String name, String number, String pipelineId, String address,String subAddress, String desc, String row, String col)
	{
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("id", id);
		parameterMap.put("name", name);
		parameterMap.put("number", number);
		parameterMap.put("pipelineId", pipelineId);
		parameterMap.put("address", address);
		parameterMap.put("subAddress", subAddress);
		parameterMap.put("desc", desc);
		parameterMap.put("row", row);
		parameterMap.put("col", col);
		
		return getMetacorpora().update(UPDATE_STATION,parameterMap);
	}
	
   public int updateAddress(String oldAddress, String newAddress)
    {
        Map<String, Object> parameterMap = new HashMap<String, Object>();
        parameterMap.put("address", oldAddress);
        parameterMap.put("newAddress", newAddress);
        
        return getMetacorpora().update(UPDATE_ADDRESS,parameterMap);
    }

	/**
	 * 根据pipelineId查询工位
	 * @param pipelineId
	 * @param start
	 * @param length
	 * @return
	 */
	public List<Station> listStation4Page(long pipelineId, int start, int length) {
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("pipelineId", pipelineId);
		parameterMap.put("start", start);
		parameterMap.put("length", length);
		
		return getMetacorpora().selectList(LIST_STATION_4_PAGE,parameterMap);
	}
	
	/**
	 * 根据pipelineId查询工位
	 * @param pipelineId
	 * @param start
	 * @param length
	 * @return
	 */
	public int listStation4PageCount(long pipelineId) {
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("pipelineId", pipelineId);
		
		return getMetacorpora().selectOne(LIST_STATION_4_PAGE_COUNT,parameterMap);
	}
	/**
	 * 获取所有设备地址
	 * @return
	 */
	public List<String> findAllAddress()
	{
		return getMetacorpora().selectList(FIND_ALL_ADDRESS);
	}
	
	public Station queryStationByAddress(String address,String subAddress)
	{
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("address", address);
		parameterMap.put("subAddress", subAddress);
		
		return getMetacorpora().selectOne(QUERY_STATION_BY_ADDRESS,parameterMap);
	}
}
