<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<%=basePath%>css/main.css" rel="stylesheet" type="text/css" />
<!--javascript-->
<title>欢迎登录监控平台</title>
<script type="text/javascript">
window.onload=function(){
	document.getElementById("username").focus();
}
</script>

</head>
<body>
<div id="login_body">
	<div style="height:221px;margin:0px;float:left; width:100%; min-width: 1200px;">
	<img src="<%=basePath%>images/index_logo.png" style="width:736px; height: 221px; border: 0px; float: left;"></img>
	<img src="<%=basePath%>images/index_logo2.png" style="width:184px; height: 221px; border: 0px; float: right; margin-right: 80px;"></img>
	</div>
	
	<div style="height: auto; width: 100%; min-width: 1200px; float: left;">
	<div id="login_body_content" class="body_center">
	<form action="<%=basePath%>j_spring_security_check" method="post">
		<div class="halign_center"><label>帐户：</label> <input id="username" type="text" name='j_username' class="input_onblur" onblur="this.className='input_onblur'" onfocus="this.className='input_onfocus'"></input></div>
		<div class="halign_center" style="margin-top:24px;"><span>密码：</span> <input type="password" name='j_password' class="input_onblur" onblur="this.className='input_onblur'" onfocus="this.className='input_onfocus'"></input></div>
		<div class="login_button"><input type="submit" value="立 即 登 陆" style="cursor:pointer"></input></div>
	</form>
	</div>
	<!--  img src="<%=basePath%>images/index_scene.png" style="width:500px; height: 334px; border: 0px;position:absolute;right: 60px;bottom:100px;"></img-->
	</div>
</div>

</body>
</html>


