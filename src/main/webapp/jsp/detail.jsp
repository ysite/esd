<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>欢迎登录监控平台</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link type="text/css" rel="stylesheet" href="<%=basePath%>css/reset.css" />
	<link type="text/css" rel="stylesheet" href="<%=basePath%>css/style.css" />
	<script type="text/javascript" src="<%=basePath%>js/jquery-latest.min.js"></script>
	<script type="text/javascript" src="<%=basePath %>js/menu.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.whpage.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/public.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/echarts-all.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/map.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/detail.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/summary2.js"></script>
	
	<script type="text/javascript">
		var checkedIndex= 1;
		var basePath = '<%=basePath%>';
	</script>
	
	<style type="text/css">
	table th{font-weight: bold;}
	div.img{
	cursor:pointer;
	height:270px;
	width:28%;
	float:left;
	margin-top:5px;
	margin-left:40px;
	margin-bottom:10px;
	border:1px solid;
	background-repeat:no-repeat;
}
	</style>
</head>
<body>
<div class="outer-header">
		<div class="inner-banner-user" style="width: 200px; height:100px; background-image: url('../images/index_logo2.png'); background-repeat: no-repeat; background-position: 0px -20px; background-size:cover;">
		</div>
</div>

<div class="ny-header" id="ny-header">
	<div class="ny-header-z">
		  <ul class="ny-menu">
          	<c:forEach items="${menus}" var="menu" varStatus="status">
			<li  id="${status.index}"><a href="<%=basePath %>${menu.url}" target="_self">${menu.name}</a>
			<c:if test="${menu.subMenus==null}">
				<ul class="sub-menu">
					<c:forEach items="${menu.subMenus}" var="subMenu" varStatus="subStatus">
						<li  id="SI${subStatus.index}"><a id="S${menu.id}" href="<%=basePath %>${menu.url}" target="_self">${menu.name}</a></li>
					</c:forEach>
				</ul>
			</c:if>
			</li>
			</c:forEach>
          </ul>
          <span class="ny-logo" ><button type="button"  id="startRoll" class="btn btn-default" style="background-color:#A90135;color:white;margin-top:16px;">滚屏</button></span>
          <ul class="ny-nav"> 
            <li><span class="ny-icons"></span><span class="ny-yhm"></span></li>
            <li><span class="ny-fgx"></span></li>
            <li><span class="ny-icons ny-icons2"></span><a href="<%=basePath%>logout" class="ny-tc">退出</a></li>
          </ul>
      </div>
 </div>

<div class="ny-container">
<input type="hidden" id="pipelineId" value="${pipelineId}"/>
<div style="display:none;" id="pipelineIds">
<c:forEach items="${stationsMap}" var="stations" varStatus="status">
${stations.key},
</c:forEach>
</div>
<c:forEach items="${stationsMap}" var="stations" varStatus="status">

<div class="list-line-img-summary" style="display:block">
  <div id="content_${stations.key}_" style="margin:0;padding:0;width:100%;height:100%;" >
  	该周期尚未采集到数据，请确认设备正常。
  </div>
</div>

<c:set value="${fn:length(stations.value)}" var="stationLength"></c:set>
<c:set value="48" var="numberr"></c:set> 
<c:set value="${stations.value[stationLength-1].number}" var="maxNumber"></c:set> 
<div class="list-container" id="list-container-${status.index+1}" style="display:block">
	<div class="list01" style="height:440px;">
	<h2 id="station-name-${stations.value[stationLength-1].pipelineId}" style="float:left; font-size:20px; color: red; text-indent:15px; margin-top:5px;"></h2>
		<div class="tr-proline-top">
			<table style="height:25px; ">
				<tr>
					<c:set value="0" var="isTrue1"></c:set>
					<c:forEach begin="1" end="${numberr}" varStatus="vstatus">
						<c:forEach items="${stations.value}" var="station">
						<c:if test="${vstatus.index==station.col && station.row==1}">
							<td style="position:relative; top: 7px;">
								<c:choose>
									<c:when test="${station.state==0}">
										${station.number}<br />
										<img src="<c:url value='/images/gray.png' />" alt="工位${station.number}"  id="station_image_${station.pipelineId}_${station.number}"/>
									</c:when>
									<c:when test="${station.state==1}">
										${station.number}<br />
										<img src="<c:url value='/images/green.png' />" alt="工位${station.number}"  id="station_image_${station.pipelineId}_${station.number}"/>
									</c:when>
									<c:when test="${station.state==-1}">
										${station.number}<br />
										<img src="<c:url value='/images/red.png' />" alt="工位${station.number}"  id="station_image_${station.pipelineId}_${station.number}"/>
									</c:when>
							    </c:choose>
						   		<c:set value="1" var="isTrue1"></c:set>
						   </td>
						</c:if>
					   </c:forEach>
					   
					   <c:if test="${isTrue1 == 0 }">
					   	<td></td>
					   </c:if>
					   
					   <c:if test="${isTrue1 == 1 }">
					   	<c:set value="0" var="isTrue1"></c:set>
					   </c:if>
					  
					</c:forEach>
				</tr>
				</table>
				<div style="height: 30px; width:98%; background-color: #ccc;"><img src="<c:url value='/images/nav.png' />" style="float: right;position:relative; left:30px;" /></div>
				<table style="height:25px; ">
				<tr>
					<c:set value="0" var="isTrue2"></c:set>
					<c:forEach begin="1" end="${numberr}" varStatus="vstatus">
						<c:forEach items="${stations.value}" var="station">
						<c:if test="${vstatus.index==station.col && station.row==2}">
							<td style="position:relative; bottom: 7px;">
								<c:choose>
									<c:when test="${station.state==0}">
										<img src="<c:url value='/images/gray.png' />" alt="工位${station.number}"  id="station_image_${station.pipelineId}_${station.number}"/>
										<br />${station.number}
									</c:when>
									<c:when test="${station.state==1}">
										<img src="<c:url value='/images/green.png' />" alt="工位${station.number}"  id="station_image_${station.pipelineId}_${station.number}"/>
										<br />${station.number}
									</c:when>
									<c:when test="${station.state==-1}">
										<img src="<c:url value='/images/red.png' />" alt="工位${station.number}"  id="station_image_${station.pipelineId}_${station.number}"/>
										<br />${station.number}
									</c:when>
							    </c:choose>
						   		<c:set value="1" var="isTrue2"></c:set>
						   </td>
						</c:if>
					   </c:forEach>
					   
					   <c:if test="${isTrue2 == 0 }">
					   	<td></td>
					   </c:if>
					   
					   <c:if test="${isTrue2 == 1 }">
					   	<c:set value="0" var="isTrue2"></c:set>
					   </c:if>
					  
					</c:forEach>
				</tr>
				
			</table>
		</div>
		<div style="height:300px;">
			<div class="list-line-img-detail">
				<div  id="content_${stations.key}" style="padding:2px;margin:0;width:100%;height:100%;">
				<h2>在线概览展示</h2>
				</div>
			</div>
			<fmt:formatNumber type="number" value="${stationLength/9}" maxFractionDigits="0" var="baseLoop"></fmt:formatNumber>
			<div class="tr-proline-bottom" style="width: 65% ; overflow: hidden;">
				<table style="table-layout:fixed; ">
					<thead>
						<tr>
							<th style="width:32px;">编号</th><th>名称</th>
							<th style="width:32px;">编号</th><th>名称</th>
							<th style="width:32px;">编号</th><th>名称</th>
							<th style="width:32px;">编号</th><th>名称</th>
							<th style="width:32px;">编号</th><th>名称</th>
							<th style="width:32px;">编号</th><th>名称</th>
							<th style="width:32px;">编号</th><th>名称</th>
							<th style="width:32px;">编号</th><th>名称</th>
							<th style="width:32px;">编号</th><th>名称</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach  begin="1" end="${stationLength%9>0 ? baseLoop+1 : baseLoop}" varStatus="status">
							<tr>
							<c:forEach items="${stations.value}" begin="${(status.index-1)*9}" end="${status.index*9-1}" var="station">
								<c:choose>
									<c:when test="${station.state==-1}">
									<td style="background-color:#ff0000" id="station_number_${station.pipelineId}_${station.number}" style="text-align:center;">${station.number}</td>
									<td style="background-color:#ff0000;white-space:nowrap;overflow: hidden; text-overflow: ellipsis;" id="station_name_${station.pipelineId}_${station.number}">${station.name}</td>
									</c:when>
									<c:when test="${station.state==0}">
									<td style="background-color:gray" id="station_number_${station.pipelineId}_${station.number}" style="text-align:center;">${station.number}</td>
									<td style="background-color:gray;white-space:nowrap;overflow: hidden; text-overflow: ellipsis;" id="station_name_${station.pipelineId}_${station.number}">${station.name}</td>
									</c:when>
									
									<c:otherwise>
									<td id="station_number_${station.pipelineId}_${station.number}" style="text-align:center;">${station.number}</td>
									<td style="white-space:nowrap;overflow: hidden; text-overflow: ellipsis;" id="station_name_${station.pipelineId}_${station.number}">${station.name}</td>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</c:forEach>
</div>


</body>
</html>
