<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>欢迎登录监控平台</title>
<link type="text/css" rel="stylesheet" href="<%=basePath%>css/reset.css" />
<link type="text/css" rel="stylesheet" href="<%=basePath%>css/style.css" />

<script type="text/javascript" src="<%=basePath%>js/jquery-latest.min.js"></script>
<script type="text/javascript" src="<%=basePath%>js/jquery.whpage.js"></script>
<script type="text/javascript" src="<%=basePath%>js/public.min.js"></script>
<script type="text/javascript" src="<%=basePath%>js/echarts-all.js"></script>
<script type="text/javascript" src="<%=basePath%>js/menu.js"></script>
<script type="text/javascript" src="<%=basePath%>js/summary.js"></script>

<script language="javascript">
    var checkedIndex= 0;
    var basePath = '<%=basePath%>';
</script>

</head>
<body>

<div class="outer-header">
	<div class="inner-banner">
		<div class="inner-banner-user" style="width: 200px; height:100px; background-image: url('../images/index_logo2.png'); background-repeat: no-repeat; background-position: 0px -20px; background-size:cover;">
		</div>
	</div>
</div>

<div class="ny-header">
	<div class="ny-header-z">
		<ul class="ny-menu">
			<c:forEach items="${menus}" var="menu" varStatus="status">
				<li id="${status.index}"><a href="<%=basePath %>${menu.url}"
					target="_self">${menu.name}</a> <c:if
						test="${menu.subMenus==null}">
						<ul class="sub-menu">
							<c:forEach items="${menu.subMenus}" var="subMenu"
								varStatus="subStatus">
								<li id="SI${subStatus.index}"><a id="S${menu.id}"
									href="<%=basePath %>${menu.url}" target="_self">${menu.name}</a></li>
							</c:forEach>
						</ul>
					</c:if></li>
			</c:forEach>
		</ul>
		<div class="ny-logo"></div>
		<ul class="ny-nav">
			<li><span class="ny-icons"></span><span class="ny-yhm"></span></li>
			<li><span class="ny-fgx"></span></li>
			<li><span class="ny-icons ny-icons2"></span><a
				href="<%=basePath%>logout" class="ny-tc">退出</a></li>
		</ul>
	</div>
</div>

<c:set value="${fn:length(pipelines)}" var="pipelineLength"></c:set> 
<fmt:formatNumber type="number" value="${pipelineLength/3}" maxFractionDigits="0" var="baseLoop"></fmt:formatNumber>
<div class="ny-container">
	<c:forEach  begin="1" end="${stationLength%3>0 ? baseLoop+1 : baseLoop}" varStatus="status">
		<c:forEach var="pipeline" items="${pipelines}" begin="${(status.index-1)*3}" end="${status.index*3-1}">
		<div class="list-line-img-summary">
	 	  <div id="content_${pipeline.id}" style="margin:0;padding:0;width:100%;height:100%;" >
	 	  	该周期尚未采集到数据，请确认设备正常。
	 	  </div>
	 	</div>
 		</c:forEach>
	</c:forEach>
</div>

</body>
</html>
