var ts = 1,interval = 30;
var refreshTimer=setInterval("refresh()",1000 * interval);
var isShowSummary = false;
var t = 0;
var times=0, timeNow=0,last=0;

$(function(){
	$("#startRoll").on('click',startRoll);
	$('.ny-container').mousedown(myMouseDown);
	drawChart();
	
	$(".list-line-img-summary").each(function(){
		$(this).css("display","none");
	});
	
	var pipelineId=$.trim($("#pipelineId").val());
	
	if('' != pipelineId)
	{
		$(".list-container").each(function(){
			$(this).css("display","none");
		});
		$('#'+pipelineId).parent().parent().parent().parent().css('display','block');
	}
	
});

function startRoll()
{
	clearInterval(refreshTimer);
	d_refresh();
	refreshTimer=setInterval("d_refresh()",1000 * interval);
}


function refreshPage()
{
	var pipelineId=$.trim($("#pipelineId").val());
	var data = {pipelineId:pipelineId};
    $.ajax({
		type:'POST',
		url:basePath+'user/refreshpipeline.html',
		data:data,
		dataType:'json',
		timeout:10000,
		success:function(data){
			for(var i=0; i<data["result"].length; i++)
			{
				if(data["result"][i].state==-1)
				{
					$("#station_name_"+data["result"][i].pipelineId+"_"+data["result"][i].number).css("background-color", "#ff0000");
					$("#station_number_"+data["result"][i].pipelineId+"_"+data["result"][i].number).css("background-color", "#ff0000");
					$("#station_number_"+data["result"][i].pipelineId+"_"+data["result"][i].number).css("text-align","center");
					$("#station_image_"+data["result"][i].pipelineId+"_"+data["result"][i].number).attr("src",basePath+"images/red.png");
				}
				else if(data["result"][i].state==1)
				{
					$("#station_name_"+data["result"][i].pipelineId+"_"+data["result"][i].number).css("background-color", "#ffffff");
					$("#station_number_"+data["result"][i].pipelineId+"_"+data["result"][i].number).css("background-color", "#ffffff");
					$("#station_number_"+data["result"][i].pipelineId+"_"+data["result"][i].number).css("text-align","center");
					$("#station_image_"+data["result"][i].pipelineId+"_"+data["result"][i].number).attr("src",basePath+"images/green.png");
				}
				else
				{
					$("#station_name_"+data["result"][i].pipelineId+"_"+data["result"][i].number).css("background-color", "gray");
					$("#station_number_"+data["result"][i].pipelineId+"_"+data["result"][i].number).css("background-color", "gray");
					$("#station_number_"+data["result"][i].pipelineId+"_"+data["result"][i].number).css("text-align","center");
					$("#station_image_"+data["result"][i].pipelineId+"_"+data["result"][i].number).attr("src",basePath+"images/gray.png");
				}
			}
		},
		error:function(request,status,error){
			window.location.reload();
		}	   
    }); 
}


function d_refresh(){
	$("#ny-header").css("display","none");
	$(".ny-container").css("padding-top","10px");
	if((t++)%4 == 0)
	{
		showSummary();
	}
	else
	{
		if(isShowSummary)
		{
			hideSummary();
		}
		drawDetail();
	}
}


function refresh()
{
	refreshPage();
	drawChart();
}

function showSummary(){
	$(".list-container").each(function(){
		$(this).css("display","none");
	});
	
	$(".list-line-img-summary").each(function(){
		$(this).css("display","block");
	});
	
	drawChart();
	
	isShowSummary = true;
}

function hideSummary(){
	$(".list-line-img-summary").each(function(){
		$(this).css("display","none");
	});
	
	$(".list-container").each(function(){
		$(this).css("display","block");
	});
	
    isShowSummary = false;
}


function drawDetail(){
	
	var containers = $(".list-container");
	
	if(times==0 && last==0)
	{	  
		var conLength = containers.length;
		times=parseInt(conLength/2);
		last=conLength%2;
	}
	
	var begin=0,end=0;
	if(timeNow==times)
	{
		begin=2*timeNow+1;
		end=last;
		timeNow=0;
	}
	
	if(timeNow < times)
	{
		begin=2*timeNow+1;
		timeNow++;
		end=2*timeNow;
	}

	for(var i=1; i <= containers.length; i++)
	{
		var elemId="#list-container-"+i;
		$(elemId).css("display","none");
	}
		
	for(var i=begin; i<=end;i++)
	{
		var elemId1="#list-container-"+i;
		$(elemId1).css("display","block");
	}
	
	refreshPage();
	drawChart();
}

function myMouseDown(){
	clearInterval(refreshTimer);
	
	times=0;
    last=0;
    timeNow=0;
    isShowSummary = false;
    t=0;
	
	$("#ny-header").css("display","block");
	$(".ny-container").css("padding-top","75px");
	
//	var containers = $(".list-container");
//	for(var i=1; i<= containers.length;i++)
//	{
//		var elemId="#list-container-"+i;
//		//$(elemId).css("display","block");
//		$(elemId).fadeIn('slow');
//	}
	
	hideSummary();
	
	refreshPage();
    drawChart();

    refreshTimer=setInterval("refresh()",1000 * interval);
};

//document.onkeydown =  function(){
//	clearInterval(refreshTimer);
//	
//	times=0;
//    last=0;
//    timeNow=0;
//    isShowSummary = false;
//    t=0;
//	
//    $("#ny-header").css("display","block");
//	$("#roll_button").css("display","block");
//	$(".ny-container").css("padding-top","75px");
//	
////	var containers = $(".list-container");
////	for(var i=1; i<= containers.length;i++)
////	{
////		var elemId="#list-container-"+i;
////		//$(elemId).css("display","block");
////		$(elemId).fadeIn('slow');
////	}
//	hideSummary();
//	
//	refreshPage();
//    drawChart();
//   
//    refreshTimer=setInterval("refresh()",1000 * interval);
//};
