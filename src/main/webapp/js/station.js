$(function(){
	queryStation(null,$("#is_admin").val());
	$("#searchStation").on('click',searchStation);
	$("#addStation").on('click',addStation);
});

function searchStation()
{
	var keyword = $.trim($("#keyword").val());
//	if(keyword==''){
//		MU.msgTips('warn','请输入要搜索的用户名');
//		$("#keyword").focus();
//		return;
//	}
	
	queryStation(keyword,$("#is_admin").val());
}

function refreshStation()
{
	var keyword = $.trim($("#keyword").val());
	if(keyword == '')
	{
		queryStation(null,$("#is_admin").val());
	}
	else
	{
		queryStation(keyword,$("#is_admin").val());
	}
}

function addStation()
{
	$.ajax({
		type:'POST',
		url:basePath+'engineer/pipelineQuery.html',
		data:{},
		dataType:'json',
		timeout:100000,
		success:function(data){
			var html='<div class="dbxx-tcc"><div class="dbxx-tcc-cont">';
		    html+='<p><span class="dbxx-tcc-wz">工位名称：</span><input type="text" id="stationName" value="" placeholder="工位名称" class="dbxx-tcc-wb" /></p>';
			html+='<p><span class="dbxx-tcc-wz">工位编号：</span><input type="text" id="stationNumber" value="" placeholder="工位编号" class="dbxx-tcc-wb" /></p>';
		    html+='<p><span class="dbxx-tcc-wz">归属流水线：</span><select id="pipelineId" value="" class="dbxx-tcc-wb">';
		    var jsonData=data.jsonData;
	        for(var i in jsonData){
	       		html += '<option value="'+jsonData[i].id+'">'+jsonData[i].name+'</option>';
	        }
	        html += '</select></p>';
		    html+='<p><span class="dbxx-tcc-wz">设备地址：</span><input type="text" id="address" value="" placeholder="设备地址" class="dbxx-tcc-wb" /></p>';
		    html+='<p><span class="dbxx-tcc-wz">传感器地址：</span><input type="text" id="subAddress" value="" placeholder="传感器地址" class="dbxx-tcc-wb" /></p>';
		    html+='<p><span class="dbxx-tcc-wz">行号：</span><input type="text" id="row"  value="" placeholder="流水线位置(行号1上2下)" class="dbxx-tcc-wb" /></p>';
		    html+='<p><span class="dbxx-tcc-wz">列号：</span><input type="text" id="col" value="" placeholder="流水线位置(列号48列)" class="dbxx-tcc-wb" /></p>';
		    html+='<p style="height:120px;"><span class="dbxx-tcc-wz">工位简介：</span><textarea id="desc" rows="7" value="" placeholder="工位简介" class="dbxx-tcc-wb" ></textarea></p>';
		    html+='<p class="cz-cont"><a href="javascript:void(0);" class="cz-btn" id="doAdd">添加</a><a href="javascript:void(0);" onclick="MU.close(this)" class="cz-btn">取消</a></p>';
			html+='</div></div>';
		    MU.alert(html,600,'新增工位');
		    $("#stationName").focus();
		    $("#doAdd").on('click',doAdd);
		},
		error:function(request,status,error){
			window.location.reload();
		}	   
	});
}

function doAdd()
{
	var name = $.trim($("#stationName").val());
	if(name==''){
		MU.msgTips('warn','请输入工位名称');
		$("#stationName").focus();
		return;
	}
	var number = $.trim($("#stationNumber").val());
	if(number==''){
		MU.msgTips('warn','请输入工位数字编号');
		return;
	}
	if(!MU.isNum(number)){
		MU.msgTips('warn','工位编号必须为数字');
		$("#stationNumber").focus();
		return;
	}
	var pipelineId = $.trim($("#pipelineId").val());
	if(pipelineId==''){
		MU.msgTips('warn','请选择用归属流水线');
		$("#pipelineId").focus();
		return;
	}
	var address = $.trim($("#address").val());
	if(address==''){
		MU.msgTips('warn','请输入设备地址');
		$("#address").focus();
		return;
	}
	
	var subAddress = $.trim($("#subAddress").val());
	if(subAddress==''){
		MU.msgTips('warn','请输入传感器地址');
		$("#subAddress").focus();
		return;
	}
	
	var row = $.trim($("#row").val());
	if(row=='')
	{
		MU.msgTips('warn','请输入流水线行号(上1下2)');
		$("#row").focus();
		return;
	}
	if(row>2 || row<1){
		MU.msgTips('warn','最多为两行，行号(上1下2)');
		$("#row").focus();
		return;
	}

	var col = $.trim($("#col").val());
	if(col=='')
	{
		MU.msgTips('warn','请输入流水线列号(1-48)');
		$("#col").focus();
		return;
	}
	if(col>48 || col<1){
		MU.msgTips('warn','最多为48，列号(1-48)');
		$("#col").focus();
		return;
	}

	var desc = $.trim($("#desc").val());
	
	MU.msgTips("loading","执行中。。。");
	$.ajax({
		type:'POST',
		url:basePath+'engineer/stationAdd.html',
		data:{name:name,number:number,pipelineId:pipelineId,address:address,subAddress:subAddress,desc:desc,col:col,row:row},
		dataType:'json',
		timeout:100000,
		success:function(data){
			MU.removeTips();
			if(data.success)
			{
				MU.msgTips('success',data.jsonData);
				MU.close();
				refreshStation();
			}
			else
			{
				MU.msgTips('error',data.jsonData);
				MU.close();
			}
		},
		error:function(request,status,error){
			window.location.reload();
		}	   
	});
}

function editStation() {
	var id = $(this).closest('tr').attr('data-id');
	$.ajax({
		type : 'POST',
		url : basePath + 'engineer/getStation.html',
		data : {id : id},
		dataType : 'json',
		timeout : 100000,
		success : function(data) {
			if (data.success) {
				var dataa = data.jsonData;
				var html = '<div class="dbxx-tcc"><div class="dbxx-tcc-cont"><input type="hidden" id="stationId" value="' + dataa.id + '"/>';
				html += '<p><span class="dbxx-tcc-wz">工位名称：</span><input type="text" id="name" value="' + dataa.name + '"  class="dbxx-tcc-wb" /></p>';
				html += '<p><span class="dbxx-tcc-wz">工位编号：</span><input type="text" id="number" value="' + dataa.number + '" class="dbxx-tcc-wb" /></p>';
				html += '<p><span class="dbxx-tcc-wz">流水线：</span><input type="text" readonly="readonly" style="background-color:#eee" id="pipelineName" value="' + dataa.pipelineName + '" class="dbxx-tcc-wb" /></p>';
				html += '<p><span class="dbxx-tcc-wz">设备地址：</span><input type="text" id="address" value="' + dataa.address + '" class="dbxx-tcc-wb" /></p>';
				html += '<p><span class="dbxx-tcc-wz">传感器地址：</span><input type="text" id="subAddress" value="' + dataa.subAddress + '" class="dbxx-tcc-wb" /></p>';
			    html += '<p><span class="dbxx-tcc-wz">行号：</span><input type="text" id="row" placeholder="流水线行号(上1下2)" value="' + dataa.row + '" class="dbxx-tcc-wb" /></p>';
			    html += '<p><span class="dbxx-tcc-wz">列号：</span><input type="text" id="col" placeholder="流水线列号(1-48列)" value="' + dataa.col + '" class="dbxx-tcc-wb" /></p>';
				html += '<p style="height:120px;"><span class="dbxx-tcc-wz">工位简介：</span><textarea id="desc" rows="7" value="' + dataa.desc + '" class="dbxx-tcc-wb" ></textarea></p>';

				html += '<p class="cz-cont"><a href="javascript:void(0);" class="cz-btn" id="doUpdate">修改</a><a href="javascript:void(0);" onclick="MU.close(this)" class="cz-btn">取消</a></p>';
				html += '</div></div>';
				MU.alert(html, 600, '编辑工位');
				$("#doUpdate").on('click', doUpdate);
			}
		},
		error : function(request, status, error) {
			window.location.reload();
		}
	});
}

function doUpdate()
{
	var id = $("#stationId").val();
	var name = $("#name").val();
	var number = $("#number").val();
	if(!MU.isNum(number)){
		MU.msgTips('warn','工位数字编号必须为数字');
		return;
	}
	
	var row = $.trim($("#row").val());
	if(row=='')
	{
		MU.msgTips('warn','请输入流水线行号(上1下2)');
		$("#row").focus();
		return;
	}
	if(row>2 || row<1){
		MU.msgTips('warn','最多为两行行号(上1下2)');
		$("#row").focus();
		return;
	}

	var col = $.trim($("#col").val());
	if(col=='')
	{
		MU.msgTips('warn','请输入流水线列号(1-48)');
		$("#col").focus();
		return;
	}
	if(col>48 || col<1){
		MU.msgTips('warn','最多为48，列号(1-48)');
		$("#col").focus();
		return;
	}

//	var pipelineId = $("#pipelineId").val();
	var address = $("#address").val();
	var subAddress = $("#subAddress").val();
	var desc = $("#desc").val();
	MU.msgTips("loading","执行中。。。");
	$.ajax({
		type:'POST',
		url:basePath+'engineer/stationUpdate.html',
		data:{id:id,name:name,number:number,address:address,subAddress:subAddress,desc:desc,col:col,row:row},
		dataType:'json',
		timeout:100000,
		success:function(data){
			MU.removeTips();
			MU.msgTips('success',data.jsonData);
			MU.close();
			refreshStation();
		},
		error:function(request,status,error){
			window.location.reload();
		}	   
	});
}


function deleteStation(){
	var id = $(this).closest('tr').attr('data-id');
	 MU.msgTips("loading","执行中。。。");
	 $.ajax({
		type:'POST',
		url:basePath+'engineer/stationDelete.html',
		data:{ids:id},
		dataType:'json',
		timeout:100000,
		success:function(data){
			MU.removeTips();
			MU.msgTips('success',data.jsonData);
			refreshStation();
		},
		error:function(request,status,error){
			window.location.reload();
		}	   
	}); 
}

function resetAddress()
{
	var address = $(this).closest('tr').attr('data-addr');
	var info = '请输入旧设备地址['+ address + ']的新设备地址';
	
	var html='<div class="dbxx-tcc"><div class="dbxx-tcc-cont">';
    html+='<p><span class="dbxx-tcc-wz">新设备地址：</span><input type="text" id="newAddress" value="" placeholder="设备地址" class="dbxx-tcc-wb" /></p>';
    html+='<p class="cz-cont"><a href="javascript:void(0);" class="cz-btn" id="doReset">确认</a><a href="javascript:void(0);" onclick="MU.close(this)" class="cz-btn">取消</a></p>';
	html+='</div></div>';
    MU.alert(html,600,info);
    $("#newAddress").focus();
    $("#doReset").on('click',function(){
    	var newAddress = $("#newAddress").val();
    	if(newAddress==null)
    	{
    		return;
    	}
    	if( newAddress.length==0)
    	{
    		MU.msgTips('warn','必须输入设备地址！');
    		return;
    	}
    	if(!MU.isNum(newAddress))
    	{
    		MU.msgTips('warn','设备地址必须为数字！');
    		return;
    	}
    	 MU.msgTips("loading","执行中。。。");
    	 $.ajax({
    		type:'POST',
    		url:basePath+'engineer/resetAddress.html',
    		data:{address:address,newAddress:newAddress},
    		dataType:'json',
    		timeout:100000,
    		success:function(data){
    			MU.removeTips();
    			MU.msgTips('success',data.jsonData);
    			MU.close();
    			refreshStation();
    		},
    		error:function(request,status,error){
    			window.location.reload();
    		}	   
    	}); 

    });
}


function queryStation(keyword,isAdmin){
	var data={};
	if(keyword != null)
	{
		data['keyword']=keyword;
	}
	
	var urlPath=basePath + 'engineer/stationQuery.html';
	MU.msgTips("loading","执行中。。。");
	$("#ajax_list_page").whpage({
		"url":urlPath,
		"cur" : 'cur',
		"action" : 'post',
		"data" : data,
		"showNum" : 5,
		"format" : function(msg) {
			 if(msg.success){
				MU.removeTips();
				var data = {};
				pageCount = Math.ceil(msg.total / msg.pageSize);
				data['pageCount'] = pageCount;
				data['cur'] = msg.pageNo;
				data['records'] = msg.total;
		    	var jsonData=msg.jsonData;
		    	$("#tableList").empty();
		    	if(jsonData.length==0){
		    		$("#tableList").append('<tr><td colspan="3">没有相关数据</td></tr>');
		    		return data;
		    	}
		       for(var i in jsonData){
			        var html="";
		       		var j = parseInt(i)+(msg.pageNo-1)*10 + 1;
		       		html += '<tr data-id="'+jsonData[i].id+'" data-addr="'+jsonData[i].address+'"><td>'+j+'</td>';
		       		html += '<td>'+jsonData[i].name+'</td>';
		       		html += '<td>'+jsonData[i].number+'</td>';
		       		html += '<td>'+jsonData[i].pipelineName+'</td>';
		       		var state = "未使用";
		       		if(jsonData[i].state == 1)
		       		{
		       			state = "正常";
		       		}
		       		else if(jsonData[i].state == -1)
		       		{
		       			state = "告警";
		       		}
		       		
		       		html += '<td>'+ state +'</td>';
		       		html += '<td>' +jsonData[i].address+'</td>';
		       		html += '<td>'+jsonData[i].subAddress+'</td>';
		       		html += '<td>'+jsonData[i].createTime+'</td>';
		       		html += '<td>'+jsonData[i].desc+'</td>';
		       		html+='<td><a href="javascript:void(0);" class="updateAddress">更新设备&nbsp;&nbsp;</a><a class="editStation" href="javascript:;" class="cz-btn" >编辑&nbsp;</a><a class="deleteStation" href="javascript:;" class="cz-btn" >&nbsp;删除</a></td></tr>';
		       		$("#tableList").append(html);
		       }
		       $(".editStation").on('click',editStation);
		       if(isAdmin=='true')
		       {
		    	   $(".deleteStation").on('click',deleteStation);
		       }
		       else
		       {
		    	   $(".deleteStation").hide();
		       }
		       $(".updateAddress").on('click', resetAddress);
			   return data;
			}else{
			   MU.msgTips('error',msg.jsonData);
			}
		},
		error: function(){
			window.location.reload();
		}
	});
};

