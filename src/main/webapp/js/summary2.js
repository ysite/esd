var myCharts = {};

function drawChart()
{
	//评论数的走势数据
	$.ajax({
		type: "POST",
		url:  basePath+"user/summaryData.html",
		data:{},				   
		dataType:"json",
		success: function(data){
			if(data.success)
			{
				for(var i=0;i<data.jsonData.length;i++)
				{
					var summary = data.jsonData[i];
					var alarm = summary.alarms >= 0 ? summary.alarms : 0;
					var normal = summary.normals >= 0 ? summary.normals : 0;
					var unused = summary.unuseds - summary.alarms - summary.normals;
					if(unused < 0){unused = 0;}
					var pipeline = document.getElementById('content_'+summary.pipelineId);
					var pipeline_ = document.getElementById('content_'+summary.pipelineId+"_");
					//if(pipeline==null){continue;}
					$("#station-name-"+summary.pipelineId).html(summary.pipelineName+"-Line");
					if(!myCharts['content_'+summary.pipelineId]){
						myCharts['content_'+summary.pipelineId] = echarts.init(pipeline);
					}
					
//					var myChart = echarts.init(pipeline);
//					if(pipeline_.parentNode.style.display=='block'){
//						myChart = echarts.init(pipeline_);
//					}
					if(!myCharts['content_'+summary.pipelineId+"_"]){
						myCharts['content_'+summary.pipelineId+"_"] = echarts.init(pipeline_);
					}
//					var myChart_ = echarts.init(pipeline_);
					var option = {
						legend: {
					        orient: 'horizontal',
					        x: 'center', 
					        y: 'top',
					        padding: [10, 10, 10, 10], 
					        itemGap: 20,
					        data: [
					            '正常','警报','未使用'
					        ]
					    },
					    title : {
							text: summary.pipelineName+"-Line",
							x: 'left', // 'center' | 'left' | {number},
					        y: 'bottom', // 'center' | 'bottom' | {number}
					        textStyle: {color: '#666'}
						},
						calculable:false,
						animation:true,
						xAxis : [
							        {
							            type : 'category',
							            data : ['监控结果分析（次数）']
							        }
							    ],
					    yAxis : [
					        {
					            type : 'value',
					            splitLine : {show : false} 
					        }
					    ],
					    series : [

					                {
					                	"name":"正常",
					                    "type":"bar",
					                    "barWidth": 40,
					                    "itemStyle": {
							                "normal": {
							                	"color": "green",
							                    "label" : { "show" : true,"textStyle": {"color": "green","fontWeight" :"bold"}}
							                }
							            },
					                    "data":[normal]
					                },
					                {
					                	"name":"警报",
					                    "type":"bar",
					                    "barWidth": 40,
					                    "itemStyle": {
							                "normal": {
							                	"color" : "red",
							                    "label" : { "show" : true,"textStyle": {"color": "red","fontWeight" :"bold"}}
							                }
							            },
					                    "data":[alarm]
					                },
					                {
					                	"name":"未使用",
					                    "type":"bar",
					                    "barWidth": 40,
					                    "itemStyle": {
							                "normal": {
							                	"color" : "grey",
							                    "label" : { "show" : true,"textStyle": {"color": "grey","fontWeight" :"bold"}}
							                }
							            },
					                    "data":[unused]
					                }
					    ]
			        };
					
//			        echarts.init(pipeline).setOption(option);
//			        echarts.init(pipeline_).setOption(option);
					myCharts['content_'+summary.pipelineId].setOption(option,true);
					myCharts['content_'+summary.pipelineId+"_"].setOption(option,true);
				}
			}
		},
		error:function(request,status,error){},
		async:false
	});
	
}


//var id=self.setInterval("drawChart()",1000 * 30);

