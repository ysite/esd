$(function(){
	drawChart();
	setInterval("drawChart()",1000 * 30);
	$(".list-line-img-summary").on('click',function(){
		var div = $(this).children().eq(0);
		window.location.href=basePath+'user/detail.html?pipelineId='+div.attr("id");
	});
});

var myCharts = {};

function drawChart()
{
	//评论数的走势数据
	$.ajax({
		type: "POST",
		url:  basePath+"user/summaryData.html",
		data:{},				   
		dataType:"json",
		success: function(data){
			if(data.success)
			{
				for(var i=0;i<data.jsonData.length;i++)
				{
					var summary = data.jsonData[i];
					var alarm = summary.alarms >= 0 ? summary.alarms : 0;
					var normal = summary.normals >= 0 ? summary.normals : 0;
					var unused = summary.unuseds - summary.alarms - summary.normals;
					if(unused < 0){unused = 0;}
					
					var pipeline = document.getElementById('content_'+summary.pipelineId);
					//if(pipeline==null){continue;}
					$("#station-name-"+summary.pipelineId).html(summary.pipelineName+"-Line");
//					var myChart = echarts.init(pipeline);
					if(!myCharts['content_'+summary.pipelineId]){
						myCharts['content_'+summary.pipelineId] = echarts.init(pipeline);
					}
					var option = {
						legend: {
					        orient: 'horizontal',
					        x: 'center', 
					        y: 'top',
					        padding: [10, 10, 10, 10], 
					        itemGap: 20,
					        data: [
					            '正常','警报','未使用'
					        ]
					    },
					    title : {
							text: summary.pipelineName+"-Line",
							x: 'left', // 'center' | 'left' | {number},
					        y: 'bottom', // 'center' | 'bottom' | {number}
					        textStyle: {color: '#666'}
						},
						calculable:false,
						animation:true,
						xAxis : [
							        {
							            type : 'category',
							            data : ['监控结果分析（次数）']
							        }
							    ],
					    yAxis : [
					        {
					            type : 'value',
					            splitLine : {show : false} 
					        }
					    ],
					    series : [

					                {
					                	"name":"正常",
					                    "type":"bar",
					                    "barWidth": 40,
					                    "itemStyle": {
							                "normal": {
							                	"color": "green",
							                    "label" : { "show" : true,"textStyle": {"color": "green","fontWeight" :"bold"}}
							                }
							            },
					                    "data":[normal]
					                },
					                {
					                	"name":"警报",
					                    "type":"bar",
					                    "barWidth": 40,
					                    "itemStyle": {
							                "normal": {
							                	"color" : "red",
							                    "label" : { "show" : true,"textStyle": {"color": "red","fontWeight" :"bold"}}
							                }
							            },
					                    "data":[alarm]
					                },
					                {
					                	"name":"未使用",
					                    "type":"bar",
					                    "barWidth": 40,
					                    "itemStyle": {
							                "normal": {
							                	"color" : "grey",
							                    "label" : { "show" : true,"textStyle": {"color": "grey","fontWeight" :"bold"}}
							                }
							            },
					                    "data":[unused]
					                }
					    ]
			        };
//			        echarts.init(pipeline).setOption(option);
					myCharts['content_'+summary.pipelineId].setOption(option,true);
				}
			}
		},
		error:function(request,status,error){}
	});
	
}


//var id=self.setInterval("drawChart()",1000 * 30);

