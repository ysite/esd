package com.test.gooseeker.dao;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.gooseeker.business.MonitorService;
import com.gooseeker.dao.beans.Monitor;
import com.gooseeker.dao.beans.Station;

public class MonitorServiceTest {
	ApplicationContext context;
	@Before
	public void init() {
		context = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		Assert.assertNotNull(context);
	}
	
	@Test
	public void testMonitor()
	{
		MonitorService service = (MonitorService)context.getBean("monitorServiceBean");
		Assert.assertNotNull(service);
//		long rowId = service.insertMonitorData(2, "line2", 1, "site1", new Date(), 0,"0|403");
//		Assert.assertNotEquals(0, rowId);
//		rowId = service.insertMonitorData(2, "line2", 2, "site2", new Date(), 1,"0|443");
//		Assert.assertNotEquals(0, rowId);
//		rowId = service.insertMonitorData(1, "line1", 1, "site1", new Date(), 1,"0|493");
//		Assert.assertNotEquals(0, rowId);
		
		long rowId = service.insertMonitorWithAddress(49,1, -1,"0|888");
		Assert.assertNotEquals(0, rowId);
		
		rowId = service.insertMonitorWithAddress(49,2, -1,"0|888");
		rowId = service.insertMonitorWithAddress(49,3, 0,"0|888");
		
		rowId = service.insertMonitorWithAddress(39,1, -1,"0|888");
		rowId = service.insertMonitorWithAddress(39,2, -1,"0|888");
		rowId = service.insertMonitorWithAddress(39,3, -1,"0|888");
		rowId = service.insertMonitorWithAddress(54,1, 1,"0|888");
		rowId = service.insertMonitorWithAddress(54,2, 1,"0|888");
		rowId = service.insertMonitorWithAddress(100,1, -1,"0|888");
		rowId = service.insertMonitorWithAddress(100,3, 0,"0|888");
		rowId = service.insertMonitorWithAddress(100,2, 0,"0|888");
		rowId = service.insertMonitorWithAddress(100,4, 0,"0|888");
	}
	
	
	@After
	public void destroy()
	{
		
	}
}
